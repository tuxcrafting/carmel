FASMG = fasmg
FASMGINC = ../fasmg-stuff/tools/fasmginc.sh

LATEX = xelatex
BIBTEX = bibtex

all: doc/reference.pdf

reference_src = $(shell find doc -name '*.tex' -o -name '*.bib')

doc/reference.pdf: $(reference_src)
	mkdir -p doc/output
	#cd doc; $(LATEX) -output-directory=output reference.tex
	#cd doc; $(BIBTEX) output/reference.aux
	cd doc; $(LATEX) -output-directory=output reference.tex
	cd doc; $(LATEX) -output-directory=output reference.tex

asm/test.bin: $(shell $(FASMGINC) asm/test.asm | uniq)
	$(FASMG) $< $@

.PHONY: test

test: asm/test.bin
	python -m carmel test --file $< --vcd asm/test.vcd --gtkw asm/test.gtkw
