"""Main script for using the Carmel toolchain.

Command line has the format:
    python main.py TOOL {-param|--param value}

Supported tools:
- disasm
- help
- test

Use "help tool=TOOL" to get a more detailed description.

"""

import sys

from .lib.disasm import disasm
from .lib.tester import TestSim


__all__ = [
    "main",
]


def tool_help(tool=None):
    """Tool for getting help on a tool.

    Parameters
    ----------
    tool
        Tool to get help about.

    """
    if tool is None:
        print(__doc__.strip())
    else:
        print(globals()[f"tool_{tool}"].__doc__.strip())


def tool_disasm(file=0, org="0"):
    """Disassemble a file containing Carmel code.

    Parameters
    ----------
    file
        File containing the code, defaults to standard input.
    org
        Load address of the code, defaults to 0.

    """
    with open(file, "rb") as f:
        buf = f.read()
    for (addr, instr, opcode, args) in disasm(buf, int(org, 0)):
        print("0x%016X 0o%011o %s %s" %
              (addr, instr, opcode.ljust(10, " "), args))


def tool_test(file=0, vcd=None, gtkw=None):
    """Run a test ROM.

    Parameters
    ----------
    file
        File containing the test ROM.
    vcd, gtkw
        Waveform output.

    """
    with open(file, "rb") as f:
        buf = f.read()

    sim = TestSim(vcd, gtkw)
    for i, x in enumerate(buf):
        sim.ram[i] = x

    while 1:
        try:
            sim.advance()
            for c in sim.outbuf:
                print(chr(c), end="")
            sim.outbuf = []
        except StopIteration:
            break


def print_usage():  # noqa: D103
    print(__doc__.strip())
    exit(1)


def main():  # noqa: D401
    """Main function."""
    if len(sys.argv) < 2:
        print_usage()

    tool = sys.argv[1]

    args = {}
    i = 2
    while i < len(sys.argv):
        k = sys.argv[i]
        if k.startswith("--"):
            args[k[2:]] = sys.argv[i + 1]
            i += 2
        elif k.startswith("-"):
            args[k[1:]] = True
            i += 1
        else:
            print_usage()

    tool = f"tool_{tool}"
    if tool in globals():
        globals()[tool](**args)
    else:
        print_usage()


if __name__ == "__main__":
    main()
