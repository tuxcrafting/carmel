"""Core1 simulator."""

from amaranth.sim import Settle, Simulator
from array import array
from sys import exc_info

from ..core1.pipeline import Pipeline


__all__ = [
    "CoreSim",
    "TestSim",
]


class CoreSim:
    """Carmel core1 simulator framework."""

    def __init__(self, vcd_file=None, gtkw_file=None):  # noqa: D107
        self.frag = Pipeline()
        self.sim = Simulator(self.frag)
        self.sim.add_clock(1e-6)
        self.stop = False

        def proc():
            yield self.frag.enable.eq(1)
            while 1:
                iaddr = yield self.frag.iaddr
                daddr = yield self.frag.daddr
                dout = yield self.frag.dout
                dsel = yield self.frag.dsel
                dread = yield self.frag.dread
                dwrite = yield self.frag.dwrite
                yield

                w = self.read(iaddr >> 1, 0b11111111)
                if iaddr & 1:
                    w &= 0xFFFFFFFF
                else:
                    w >>= 32
                yield self.frag.idata.eq(w)

                if dread:
                    yield self.frag.din.eq(self.read(daddr, dsel))

                if dwrite:
                    self.write(daddr, dsel, dout)

                yield Settle()

        self.sim.add_sync_process(proc)

        self.vcdctx = None
        if vcd_file is not None:
            self.vcdctx = self.sim.write_vcd(vcd_file, gtkw_file)
            self.vcdctx.__enter__()

    def read(self, addr, sel):
        """Read the specified bytes of a 64-bit word."""
        raise NotImplementedError()

    def write(self, addr, sel, data):
        """Write the specified bytes of a 64-bit word."""
        raise NotImplementedError()

    def advance(self):
        """Advance the simulation."""
        try:
            self.sim.advance()
            if self.stop:
                if self.vcdctx is not None:
                    self.vcdctx.__exit__(None, None, None)
                    self.vcdctx = None
                raise StopIteration()
        except Exception:
            if self.vcdctx is None:
                raise
            if not self.vcdctx.__exit__(*exc_info()):
                raise


class TestSim(CoreSim):
    """Test class for the core1 test ROM."""

    def __init__(self, *args):  # noqa: D107
        CoreSim.__init__(self, *args)
        self.ram = array("B", [0] * 65536)
        self.outbuf = []

    def read(self, addr, sel):  # noqa: D102
        addr &= (1 << 59) - 1
        if addr < len(self.ram) // 8:
            data = 0
            for i in range(8):
                if sel & (1 << i):
                    data |= self.ram[addr * 8 + 7 - i] << (i * 8)
            return data
        else:
            return 0

    def write(self, addr, sel, data):  # noqa: D102
        addr &= (1 << 59) - 1
        if addr < len(self.ram) // 8:
            for i in range(8):
                if sel & (1 << i):
                    self.ram[addr * 8 + 7 - i] = data >> (i * 8) & 0xFF
        elif addr == (1 << 59) - 1:
            data &= 0xFF
            if data == 4:
                self.stop = True
            else:
                self.outbuf.append(data)
        elif addr == (1 << 59) - 2:
            print("0x%016X" % data)
