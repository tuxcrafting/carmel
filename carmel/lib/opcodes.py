"""Helper opcode definitions."""

from enum import Enum, auto


__all__ = [
    "OpcodeFormat",
    "Opcode",
    "Opcodes",
    "REGS",
]


REGS = {}
REGS[0o00] = "NULL"
REGS[0o01] = "RA"
REGS[0o02] = "SP"
REGS[0o03] = "GP"
REGS[0o04] = "TP"
REGS[0o05] = "K0"
REGS[0o06] = "K1"
REGS[0o07] = "K2"
for i in range(8):
    REGS[0o10 + i] = f"T{i}"
for i in range(32):
    REGS[0o20 + i] = f"V{i}"
for i in range(16):
    REGS[0o60 + i] = f"A{i}"


class OpcodeFormat(Enum):
    """Opcode format enum."""

    R = auto()
    I = auto()
    S = auto()
    L = auto()
    X = auto()
    B = auto()


class Opcode:
    """Opcode definition.

    Parameters
    ----------
    name : str
        Opcode name.
    sys : bool
    major : int
    minor : int | None
        Opcode identifiers.
    format : OpcodeFormat
        Opcode format.
    flags : str
        Flags, "m" for memory access, "b" for branch.

    """

    def __init__(self, sys, major, minor, fmt, flags=""):  # noqa: D107
        self.name = "?"
        self.sys = sys
        self.major = major
        self.minor = minor
        self.fmt = fmt
        self.flags = flags


class Opcodes:
    """List of all opcodes."""

    ADD = Opcode(False, 0o00, 0o01, OpcodeFormat.R)
    ADDI = Opcode(False, 0o01, None, OpcodeFormat.I)
    AND = Opcode(False, 0o00, 0o02, OpcodeFormat.R)
    ANDI = Opcode(False, 0o02, None, OpcodeFormat.I)
    ANDN = Opcode(False, 0o00, 0o72, OpcodeFormat.R)
    B = Opcode(False, 0o20, None, OpcodeFormat.B, "b")
    BL = Opcode(False, 0o21, None, OpcodeFormat.L, "b")
    BLX = Opcode(False, 0o23, None, OpcodeFormat.L, "b")
    BSWAPD = Opcode(False, 0o00, 0o26, OpcodeFormat.R)
    BSWAPQ = Opcode(False, 0o00, 0o27, OpcodeFormat.R)
    BSWAPW = Opcode(False, 0o00, 0o25, OpcodeFormat.R)
    BX = Opcode(False, 0o22, None, OpcodeFormat.B, "b")
    CEQ = Opcode(False, 0o00, 0o10, OpcodeFormat.R)
    CEQI = Opcode(False, 0o10, None, OpcodeFormat.I)
    CGE = Opcode(False, 0o00, 0o15, OpcodeFormat.R)
    CGEI = Opcode(False, 0o15, None, OpcodeFormat.I)
    CGEU = Opcode(False, 0o00, 0o16, OpcodeFormat.R)
    CGEUI = Opcode(False, 0o16, None, OpcodeFormat.I)
    CGTI = Opcode(False, 0o51, None, OpcodeFormat.I)
    CGTUI = Opcode(False, 0o52, None, OpcodeFormat.I)
    CLEI = Opcode(False, 0o55, None, OpcodeFormat.I)
    CLEUI = Opcode(False, 0o56, None, OpcodeFormat.I)
    CLO = Opcode(False, 0o00, 0o63, OpcodeFormat.R)
    CLT = Opcode(False, 0o00, 0o11, OpcodeFormat.R)
    CLTI = Opcode(False, 0o11, None, OpcodeFormat.I)
    CLTU = Opcode(False, 0o00, 0o12, OpcodeFormat.R)
    CLTUI = Opcode(False, 0o12, None, OpcodeFormat.I)
    CLZ = Opcode(False, 0o00, 0o62, OpcodeFormat.R)
    CNE = Opcode(False, 0o00, 0o14, OpcodeFormat.R)
    CNEI = Opcode(False, 0o14, None, OpcodeFormat.I)
    CNTC = Opcode(False, 0o00, 0o54, OpcodeFormat.R)
    CNTEST = Opcode(False, 0o00, 0o17, OpcodeFormat.R)
    CNTESTI = Opcode(False, 0o17, None, OpcodeFormat.I)
    CNTS = Opcode(False, 0o00, 0o55, OpcodeFormat.R)
    CTEST = Opcode(False, 0o00, 0o13, OpcodeFormat.R)
    CTESTI = Opcode(False, 0o13, None, OpcodeFormat.I)
    CTO = Opcode(False, 0o00, 0o61, OpcodeFormat.R)
    CTZ = Opcode(False, 0o00, 0o60, OpcodeFormat.R)
    EXTB = Opcode(False, 0o00, 0o30, OpcodeFormat.R)
    EXTBU = Opcode(False, 0o00, 0o34, OpcodeFormat.R)
    EXTD = Opcode(False, 0o00, 0o32, OpcodeFormat.R)
    EXTDU = Opcode(False, 0o00, 0o36, OpcodeFormat.R)
    EXTW = Opcode(False, 0o00, 0o31, OpcodeFormat.R)
    EXTWU = Opcode(False, 0o00, 0o35, OpcodeFormat.R)
    JLR = Opcode(False, 0o00, 0o21, OpcodeFormat.R)
    JLRX = Opcode(False, 0o00, 0o23, OpcodeFormat.R)
    LAPC = Opcode(False, 0o77, None, OpcodeFormat.L, "m")
    LB = Opcode(False, 0o30, None, OpcodeFormat.I, "m")
    LBPC = Opcode(False, 0o70, None, OpcodeFormat.L, "m")
    LBU = Opcode(False, 0o34, None, OpcodeFormat.I, "m")
    LBUPC = Opcode(False, 0o74, None, OpcodeFormat.L, "m")
    LD = Opcode(False, 0o32, None, OpcodeFormat.I, "m")
    LDPC = Opcode(False, 0o72, None, OpcodeFormat.L, "m")
    LDU = Opcode(False, 0o36, None, OpcodeFormat.I, "m")
    LDUPC = Opcode(False, 0o76, None, OpcodeFormat.L, "m")
    LQ = Opcode(False, 0o33, None, OpcodeFormat.I, "m")
    LQPC = Opcode(False, 0o73, None, OpcodeFormat.L, "m")
    LW = Opcode(False, 0o31, None, OpcodeFormat.I, "m")
    LWPC = Opcode(False, 0o71, None, OpcodeFormat.L, "m")
    LWU = Opcode(False, 0o35, None, OpcodeFormat.I, "m")
    LWUPC = Opcode(False, 0o75, None, OpcodeFormat.L, "m")
    MOVC = Opcode(False, 0o00, 0o66, OpcodeFormat.R)
    MOVCU = Opcode(False, 0o00, 0o67, OpcodeFormat.R)
    MOVNC = Opcode(False, 0o00, 0o64, OpcodeFormat.R)
    MOVNCU = Opcode(False, 0o00, 0o65, OpcodeFormat.R)
    OR = Opcode(False, 0o00, 0o03, OpcodeFormat.R)
    ORI = Opcode(False, 0o03, None, OpcodeFormat.I)
    ORN = Opcode(False, 0o00, 0o73, OpcodeFormat.R)
    SB = Opcode(False, 0o24, None, OpcodeFormat.S)
    SBPC = Opcode(False, 0o64, None, OpcodeFormat.X)
    SD = Opcode(False, 0o26, None, OpcodeFormat.S)
    SDPC = Opcode(False, 0o66, None, OpcodeFormat.X)
    SHL = Opcode(False, 0o00, 0o05, OpcodeFormat.R)
    SHLI = Opcode(False, 0o05, None, OpcodeFormat.I)
    SHR = Opcode(False, 0o00, 0o06, OpcodeFormat.R)
    SHRI = Opcode(False, 0o06, None, OpcodeFormat.I)
    SHRU = Opcode(False, 0o00, 0o07, OpcodeFormat.R)
    SHRUI = Opcode(False, 0o07, None, OpcodeFormat.I)
    SQ = Opcode(False, 0o27, None, OpcodeFormat.S, "m")
    SQPC = Opcode(False, 0o67, None, OpcodeFormat.X, "m")
    SUB = Opcode(False, 0o00, 0o00, OpcodeFormat.R, "m")
    SW = Opcode(False, 0o25, None, OpcodeFormat.S, "m")
    SWPC = Opcode(False, 0o65, None, OpcodeFormat.X, "m")
    XOR = Opcode(False, 0o00, 0o04, OpcodeFormat.R, "m")
    XORI = Opcode(False, 0o04, None, OpcodeFormat.I, "m")
    XORN = Opcode(False, 0o00, 0o74, OpcodeFormat.R, "m")

    sys = {}
    cond = {}


for k in dir(Opcodes):
    v = getattr(Opcodes, k)
    if isinstance(v, Opcode):
        v.name = k
        t = Opcodes.sys if v.sys else Opcodes.cond
        if v.minor is None:
            t[v.major] = v
        else:
            if v.major not in t:
                t[v.major] = {}
            t[v.major][v.minor] = v
