"""Disassembler for Carmel code."""

from struct import unpack

from .opcodes import OpcodeFormat, Opcodes, REGS


__all__ = [
    "disasm",
]


def build_imm(*args):
    n = 0
    for i, arg in enumerate(args):
        n |= arg << (i * 6)
    size = len(args) * 6
    if n & (1 << (size - 1)):
        return n - (1 << size)
    else:
        return n


def format_args(op, instr, addr):
    i_f3 = instr >> 18 & 63
    i_f2 = instr >> 12 & 63
    i_f1 = instr >> 6 & 63
    i_f0 = instr >> 0 & 63

    args = "?"

    if op.fmt == OpcodeFormat.R:
        args = f"{REGS[i_f2]}, {REGS[i_f1]}, {REGS[i_f3]}"
    elif op.fmt == OpcodeFormat.I:
        imm = build_imm(i_f0, i_f1)
        if "m" in op.flags:
            args = f"{imm}({REGS[i_f2]}), {REGS[i_f3]}"
        else:
            args = f"{REGS[i_f2]}, #{imm}, {REGS[i_f3]}"
    elif op.fmt == OpcodeFormat.S:
        imm = build_imm(i_f0, i_f3)
        if "m" in op.flags:
            args = f"{REGS[i_f1]}, {imm}({REGS[i_f2]})"
    elif op.fmt == OpcodeFormat.L:
        imm = build_imm(i_f0, i_f1, i_f2)
        if "b" in op.flags:
            tgt = "0x%016X" % (addr + 4 + imm * 4)
            args = f"{tgt}, {REGS[i_f3]}"
        elif "m" in op.flags:
            tgt = "0x%016X" % (addr + 4 + imm)
            args = f"({tgt}), {REGS[i_f3]}"
    elif op.fmt == OpcodeFormat.X:
        imm = build_imm(i_f0, i_f2, i_f3)
        if "m" in op.flags:
            tgt = "0x%016X" % (addr + 4 + imm)
            args = f"{REGS[i_f3]}, ({tgt})"
    elif op.fmt == OpcodeFormat.B:
        imm = build_imm(i_f0, i_f1, i_f2, i_f3)
        if "b" in op.flags:
            tgt = "0x%016X" % (addr + 4 + imm * 4)
            args = f"{tgt}"

    return args


def decode_instr(instr, addr):
    i_cc = instr >> 30 & 3
    i_f4 = instr >> 24 & 63
    i_f0 = instr >> 0 & 63

    t = Opcodes.sys if i_cc == 0 else Opcodes.cond
    op = None
    if i_f4 in t:
        op = t[i_f4]
        if isinstance(op, dict):
            op = op.get(i_f0, None)

    args = "?"
    if op is None:
        opcode = "DD"
        args = "#0x%08X" % instr
    else:
        opcode = op.name
        if i_cc == 2:
            opcode += ".NC"
        elif i_cc == 3:
            opcode += ".C"
        args = format_args(op, instr, addr)

    return (addr, instr, opcode, args)


def disasm(buf, org):
    """Disassemble Carmel code.

    Parameters
    ----------
    buf : bytes
        Buffer containing code.
    org : int
        Load address.
    """
    for i in range(len(buf) // 4):
        instr, = unpack(">I", buf[i * 4:i * 4 + 4])
        addr = org + i * 4
        yield decode_instr(instr, addr)
