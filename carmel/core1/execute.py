"""Execute stage."""

from amaranth import C, Cat, Elaboratable, Module, Mux, Signal

from .alu import AluCond
from .microcode import G, Microcode


__all__ = [
    "Execute",
]


class Execute(Elaboratable):
    """Pipeline execution stage.

    Parameters
    ----------
    pc : Counter(62)
        Program counter.
    rs, rt : RegisterBank.ReadPort(64, 6)
        Register read ports.

    Attributes
    ----------
    ucode : Microcode(), in
        Microcode line.
    pc_p4 : Signal(62), in
        Address of this instruction plus 4 (delayed PC).
    imm : Signal(24), in
        Immediate operand.
    rd_ma, rd_wb, rd_wb1 : Signal(6), in
        Register index from memory access and writeback.
    a_ma, a_wb, a_wb1 : Signal(64), in
        Input data of memory access and writeback stage.
    cond_in : Signal(), in
        Condition code input.
    cond_out : Signal(), out
        Condition code output.
    q : Signal(64), out
        ALU output.
    vt : Signal(64), out
        Value of register t.

    """

    def __init__(self, pc, rs, rt):  # noqa: D107
        self.pc = pc
        self.rs = rs
        self.rt = rt

        self.ucode = Microcode(name="ucode")
        self.pc_p4 = Signal(62)
        self.imm = Signal(24)
        self.rd_ma = Signal(6)
        self.rd_wb = Signal(6)
        self.rd_wb1 = Signal(6)
        self.a_ma = Signal(64)
        self.a_wb = Signal(64)
        self.a_wb1 = Signal(64)
        self.cond_in = Signal()
        self.cond_out = Signal()
        self.q = Signal(64)
        self.vt = Signal(64)

        self.alu_cond = AluCond()

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        m.submodules.alu_cond = self.alu_cond

        vs = Signal(64)

        rs = G(self.ucode, "rs")
        rt = G(self.ucode, "rt")

        with m.If(rs == 0):
            m.d.comb += vs.eq(0)
        with m.Elif(rs == self.rd_ma):
            m.d.comb += vs.eq(self.a_ma)
        with m.Elif(rs == self.rd_wb):
            m.d.comb += vs.eq(self.a_wb)
        with m.Elif(rs == self.rd_wb1):
            m.d.comb += vs.eq(self.a_wb1)
        with m.Else():
            m.d.comb += vs.eq(self.rs.q)

        with m.If(rt == 0):
            m.d.comb += self.vt.eq(0)
        with m.Elif(rt == self.rd_ma):
            m.d.comb += self.vt.eq(self.a_ma)
        with m.Elif(rt == self.rd_wb):
            m.d.comb += self.vt.eq(self.a_wb)
        with m.Elif(rt == self.rd_wb1):
            m.d.comb += self.vt.eq(self.a_wb1)
        with m.Else():
            m.d.comb += self.vt.eq(self.rt.q)

        m.d.comb += self.alu_cond.op.eq(G(self.ucode, "alu_op"))
        m.d.comb += self.alu_cond.compare.eq(G(self.ucode, "compare"))
        m.d.comb += self.alu_cond.movc.eq(G(self.ucode, "movc"))

        m.d.comb += self.alu_cond.a.eq(
            Mux(G(self.ucode, "rs_pc"), self.pc_p4 << 2, vs))

        # believe it or not, the "<< 0" is necessary
        imm = Mux(G(self.ucode, "imm_shift"),
                  self.imm.as_signed() << 2,
                  self.imm.as_signed() << 2 >> 2)

        m.d.comb += self.alu_cond.b.eq(
            Mux(G(self.ucode, "rt_imm"), imm, self.vt))

        m.d.comb += self.alu_cond.cond_in.eq(self.cond_in)
        m.d.comb += self.cond_out.eq(self.alu_cond.cond_out)

        with m.If(G(self.ucode, "pc4")):
            m.d.comb += self.q.eq(self.pc_p4 << 2)
        with m.Elif(G(self.ucode, "pc8")):
            m.d.comb += self.q.eq(self.pc.q << 2)
        with m.Else():
            m.d.comb += self.q.eq(self.alu_cond.q)

        m.d.comb += self.pc.set.eq(G(self.ucode, "branch"))
        m.d.comb += self.pc.a.eq(self.alu_cond.q[2:])

        return m
