"""Instruction decoder."""

from amaranth import Array, Cat, Elaboratable, Module, Mux, Signal

from .microcode import G, ImmType, Microcode, URom


__all__ = [
    "InstructionDecoder",
]


class InstructionDecoder(Elaboratable):
    """Instruction decoder pipeline stage.

    Parameters
    ----------
    rs, rt : RegisterBank.ReadPort(64, 6)
        Register read ports.

    Attributes
    ----------
    skip : Signal(), in
        Whether to skip decoding this instruction.
    cond : Signal(), in
        Condition code from execute stage.
    instr : Signal(32), in
        Instruction bits.
    ucode : Microcode(), out
        Microcode line.
    imm : Signal(24), out
        Immediate value (should be sign-extended).

    """

    def __init__(self, pc, rs, rt):  # noqa: D107
        self.rs = rs
        self.rt = rt

        self.skip = Signal()
        self.cond = Signal()
        self.instr = Signal(32)
        self.ucode = Microcode(name="ucode")
        self.imm = Signal(24)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        i_f0 = self.instr[0:6]
        i_f1 = self.instr[6:12]
        i_f2 = self.instr[12:18]
        i_f3 = self.instr[18:24]
        i_f4 = self.instr[24:30]
        i_cc = self.instr[30:32]

        iucode = Microcode(name="iucode")
        with m.If(i_cc == 0):
            m.d.comb += iucode.eq(Array(URom.sys)[i_f4])
        with m.Else():
            with m.If(i_f4 == 0):
                m.d.comb += iucode.eq(Array(URom.minor[0o00])[i_f0])
            with m.Else():
                m.d.comb += iucode.eq(Array(URom.major)[i_f4])

        with m.If(G(iucode, "has_rd")):
            m.d.comb += G(iucode, "rd").eq(i_f3)
        with m.If(G(iucode, "has_rs")):
            m.d.comb += G(iucode, "rs").eq(i_f2)
            m.d.comb += self.rs.sel.eq(i_f2)
        with m.If(G(iucode, "has_rt")):
            m.d.comb += G(iucode, "rt").eq(i_f1)
            m.d.comb += self.rt.sel.eq(i_f1)

        with m.Switch(G(iucode, "imm")):
            with m.Case(ImmType.I):
                m.d.comb += self.imm.eq(Cat(i_f0, i_f1).as_signed())
            with m.Case(ImmType.S):
                m.d.comb += self.imm.eq(Cat(i_f0, i_f3).as_signed())
            with m.Case(ImmType.L):
                m.d.comb += self.imm.eq(Cat(i_f0, i_f1, i_f2).as_signed())
            with m.Case(ImmType.X):
                m.d.comb += self.imm.eq(Cat(i_f0, i_f2, i_f3).as_signed())
            with m.Case(ImmType.B):
                m.d.comb += self.imm.eq(Cat(i_f0, i_f1, i_f2, i_f3))

        execute = (~i_cc[1] | (i_cc[0] == self.cond)) & ~self.skip
        m.d.comb += self.ucode.eq(Mux(execute, iucode, 0))

        return m
