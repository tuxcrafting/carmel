"""Memory access stage."""

from amaranth import Cat, Elaboratable, Module, Signal

from .microcode import G, MEM_TBL, Microcode


__all__ = [
    "MemoryAccess",
]


class MemoryAccess(Elaboratable):
    """Pipeline memory access stage.

    Attributes
    ----------
    ucode : Microcode(), in
        Microcode line.
    a, vt : Signal(64), in
        Input from EX.
    q : Signal(64), in
        Output to WB.
    daddr : Signal(61), out
    dout : Signal(64), out
    dsel : Signal(8), out
    dread, dwrite : Signal(), out
        Data cache connections.

    """

    def __init__(self):  # noqa: D107
        self.ucode = Microcode(name="ucode")
        self.a = Signal(64)
        self.vt = Signal(64)
        self.q = Signal(64)
        self.daddr = Signal(61)
        self.dout = Signal(64)
        self.dsel = Signal(8)
        self.dread = Signal()
        self.dwrite = Signal()

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        m.d.comb += self.q.eq(self.a)

        m.d.comb += self.daddr.eq(self.a[3:])

        m.d.comb += self.dread.eq(G(self.ucode, "mread"))
        m.d.comb += self.dwrite.eq(G(self.ucode, "mwrite"))

        with m.Switch(Cat(self.a[:3], G(self.ucode, "msize")[:2])):
            for i in range(32):
                with m.Case(i):
                    t = MEM_TBL[i]
                    if t is None:
                        pass
                    else:
                        dsel, a, b = t
                        m.d.comb += self.dsel.eq(dsel)
                        m.d.comb += self.dout[a:b].eq(self.vt)

        return m
