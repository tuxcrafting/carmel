"""Various arithmetic and logic components."""

from amaranth import C, Cat, Elaboratable, Module, Mux, Repl, Shape, Signal


__all__ = [
    "Alu",
    "AluCond",
    "BitExtender",
    "BitSwapper",
    "LogicUnit",
]


class LogicUnit(Elaboratable):
    """Logic unit, implementing any B^2->B gate based on a programmable input.

    Parameters
    ----------
    width : int
        Width of the data operated upon.

    Attributes
    ----------
    op : Signal(4), in
        Truth table of the operation to implement.
        Bit 0 is the value of 0,0, 1 is 1,0, 2 is 0,1 and 3 is 1,1.
    a, b : Signal(width), in
        Data input.
    q : Signal(width), out
        Data output.

    """

    def __init__(self, width):  # noqa: D107
        self.width = width

        self.op = Signal(4)
        self.a = Signal(self.width)
        self.b = Signal(self.width)
        self.q = Signal(self.width)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        for i in range(self.width):
            abv = Cat(self.a[i], self.b[i])
            val = self.op.bit_select(abv, 1)
            m.d.comb += self.q[i].eq(val)

        return m


class BitExtender(Elaboratable):
    """Bit extender, allowing signed and unsigned integer extension.

    Parameters
    ----------
    min_order : int
        Minimum bit order.
    max_order : int
        Maximum bit order.

    Attributes
    ----------
    size : Signal(.), in
        Input operand size.
    signed : Signal(), in
        Whether to do a signed extension.
    a : Signal(2**max_order), in
        Input operand.
    q : Signal(2**max_order), out
        Output operand.

    """

    def __init__(self, min_order, max_order):  # noqa: D107
        self.min_order = min_order
        self.max_order = max_order

        assert self.max_order > self.min_order
        self.orders = self.max_order - self.min_order + 1
        self.max_width = 2 ** self.max_order

        self.size = Signal(Shape.cast(range(self.orders)))
        self.signed = Signal()
        self.a = Signal(2 ** self.max_order)
        self.q = Signal(2 ** self.max_order)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        with m.Switch(self.size):
            for i in range(self.orders):
                with m.Case(i):
                    order = i + self.min_order
                    width = 2 ** order
                    m.d.comb += self.q[:width].eq(self.a[:width])

                    sign = self.signed & self.a[width - 1]
                    m.d.comb += self.q[width:].eq(sign.as_signed())

        return m


class BitSwapper(Elaboratable):
    """Bit swapper, allowing endianness swapping.

    Parameters
    ----------
    min_order : int
        Minimum bit order.
    max_order : int
        Maximum bit order.

    Attributes
    ----------
    size : Signal(.), in
        Input operand size.
    a : Signal(2**max_order), in
        Input operand.
    q : Signal(2**max_order), out
        Output operand.

    """

    def __init__(self, min_order, max_order):  # noqa: D107
        self.min_order = min_order
        self.max_order = max_order

        assert self.max_order > self.min_order
        self.orders = self.max_order - self.min_order + 1
        self.min_width = 2 ** self.min_order
        self.max_width = 2 ** self.max_order

        self.size = Signal(Shape.cast(range(self.orders)))
        self.a = Signal(2 ** self.max_order)
        self.q = Signal(2 ** self.max_order)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        with m.Switch(self.size):
            for i in range(self.orders):
                with m.Case(i):
                    order = i + self.min_order
                    width = 2 ** order
                    mw = self.min_width
                    for j in range(2 ** i):
                        a = j * mw
                        b = width - mw - a
                        m.d.comb += self.q[a:a + mw].eq(self.a[b:b + mw])
                    m.d.comb += self.q[width:].eq(0)

        return m


class Alu(Elaboratable):
    """64-bit arithmetic and logic unit.

    Attributes
    ----------
    op : Signal(7), in
        ALU operation.
    a, b : Signal(64), in
        Input operands.
    q : Signal(64), out
        Output operand.
    eqz : Signal(), out
        Result is equal to 0 (plain equality for subtraction/XOR).
    lt, ltu : Signal(), out
        Comparison result for subtraction.

    Operations
    ----------

    00-llll
        Logic unit operation.
    01cllll
        Addition, a + lu(a, b) + c.
    1001-01
        Shift left.
    1001-10
        Shift right signed.
    1001-11
        Shift right unsigned.
    1010xss
        Zero (x=0) or sign (x=1)-extend a with size s.
    1011-ss
        Swap the bytes in a in a word of size s.
    1100n00
        Negate a if n, then count set bits in a.
    1100n01
        Negate a if n, then count consecutive trailing zero bits in a.
    1100n10
        Negate a if n, then count consecutive leading zero bits in a.

    """

    def __init__(self):  # noqa: D107
        self.op = Signal(7)
        self.a = Signal(64)
        self.b = Signal(64)
        self.q = Signal(64)
        self.eqz = Signal()
        self.lt = Signal()
        self.ltu = Signal()

        self.lu = LogicUnit(64)
        self.ext = BitExtender(3, 6)
        self.swap = BitSwapper(3, 6)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        m.submodules.lu = self.lu
        m.submodules.ext = self.ext
        m.submodules.swap = self.swap

        m.d.comb += self.lu.op.eq(self.op[:4])
        m.d.comb += self.lu.a.eq(self.a)
        m.d.comb += self.lu.b.eq(self.b)

        m.d.comb += self.ext.size.eq(self.op[:2])
        m.d.comb += self.ext.signed.eq(self.op[2])
        m.d.comb += self.ext.a.eq(self.a)

        m.d.comb += self.swap.size.eq(self.op[:2])
        m.d.comb += self.swap.a.eq(self.a)

        add_q = (self.a + self.lu.q + self.op[4])[:65]
        ov = (self.a[63] == self.lu.q[63]) & (self.a[63] != add_q[63])

        neg_a = (self.a ^ Repl(self.op[2], 64))

        with m.Switch(self.op):
            with m.Case("00-----"):
                m.d.comb += self.q.eq(self.lu.q)
            with m.Case("01-----"):
                m.d.comb += self.q.eq(add_q[:64])
            with m.Case("1001-01"):
                m.d.comb += self.q.eq((self.a << self.b[:6])[:64])
            with m.Case("1001-10"):
                m.d.comb += self.q.eq((self.a.as_signed() >> self.b[:6])[:64])
            with m.Case("1001-11"):
                m.d.comb += self.q.eq((self.a >> self.b[:6])[:64])
            with m.Case("1010---"):
                m.d.comb += self.q.eq(self.ext.q)
            with m.Case("1011---"):
                m.d.comb += self.q.eq(self.swap.q)
            with m.Case("1100-00"):
                t = C(0)
                for i in range(64):
                    t += neg_a[i]
                m.d.comb += self.q.eq(t)
            with m.Case("1100-01"):
                t = C(64)
                for i in range(64):
                    j = 63 - i
                    t = Mux(neg_a[j], j, t)
                m.d.comb += self.q.eq(t)
            with m.Case("1100-10"):
                t = C(64)
                for i in range(64):
                    j = 63 - i
                    t = Mux(neg_a[i], j, t)
                m.d.comb += self.q.eq(t)

        m.d.comb += self.eqz.eq(self.q == 0)
        m.d.comb += self.lt.eq(ov ^ add_q[63])
        m.d.comb += self.ltu.eq(~add_q[64])

        return m


class AluCond(Elaboratable):
    """ALU including condition code handling.

    Attributes
    ----------
    op : Signal(7), in
        ALU operation.
    compare : Signal(5), in
        Comparison type (format: enable, lt, unsigned, eq, invert).
    movc : Signal(3), in
        MOVC instruction family (format: set, signed, invert).
    a, b : Signal(64), in
        Input operands.
    cond_in : Signal(), in
        Condition code input.
    q : Signal(64), out
        Output operand.
    cond_out : Signal(), out
        Condition code output.

    """

    def __init__(self):  # noqa: D107
        self.op = Signal(7)
        self.compare = Signal(5)
        self.movc = Signal(3)
        self.a = Signal(64)
        self.b = Signal(64)
        self.cond_in = Signal()
        self.q = Signal(64)
        self.cond_out = Signal()

        self.alu = Alu()

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        m.submodules.alu = self.alu

        m.d.comb += self.alu.op.eq(self.op)
        m.d.comb += self.alu.a.eq(self.a)
        m.d.comb += self.alu.b.eq(self.b)

        cmp_lt = Mux(self.compare[2], self.alu.ltu, self.alu.lt)
        cmp = self.alu.eqz & self.compare[1]
        cmp |= cmp_lt & self.compare[3]
        cmp ^= self.compare[0]

        with m.If(self.compare[4]):
            m.d.comb += self.q.eq(cmp)
            m.d.comb += self.cond_out.eq(cmp)
        with m.Else():
            c = self.cond_in ^ self.movc[0]
            c = Mux(self.movc[1], Repl(c, 64), c)
            m.d.comb += self.q.eq(Mux(self.movc[2], c, self.alu.q))
            m.d.comb += self.cond_out.eq(self.cond_in)

        return m
