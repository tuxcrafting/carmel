"""Primary pipeline."""

from amaranth import Elaboratable, Module, Mux, Signal

from .decode import InstructionDecoder
from .execute import Execute
from .memacc import MemoryAccess
from .microcode import G
from .regs import Counter, RegisterBank
from .writeback import Writeback


__all__ = [
    "Pipeline",
]


class Pipeline(Elaboratable):
    """Processor pipeline as a monolithic module.

    Attributes
    ----------
    enable : Signal(), in
        Whether to continue fetching instructions.
    iaddr : Signal(62), out
        Instruction cache address.
    idata : Signal(32), in
        Instruction cache data.
    daddr : Signal(61), out
        Data cache address.
    din : Signal(64), in
        Data cache data in.
    dout : Signal(64), out
        Data cache data out.
    dsel : Signal(8), out
        Data cache selector.
    dread : Signal(), out
        Data cache read.
    dwrite : Signal(), out
        Data cache write.

    """

    def __init__(self):  # noqa: D107
        self.enable = Signal()
        self.iaddr = Signal(62)
        self.idata = Signal(32)
        self.daddr = Signal(61)
        self.din = Signal(64)
        self.dout = Signal(64)
        self.dsel = Signal(8)
        self.dread = Signal()
        self.dwrite = Signal()

        self.pc = Counter(62, 1, 3 << 60)
        self.regbank = RegisterBank(64, 6, 2, 1)

        self.id = InstructionDecoder(self.pc, *self.regbank.read[:2])
        self.ex = Execute(self.pc, *self.regbank.read[:2])
        self.ma = MemoryAccess()
        self.wb = Writeback(self.regbank.write[0])

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        m.submodules.pc = self.pc
        m.submodules.regbank = self.regbank

        m.submodules.id = self.id
        m.submodules.ex = self.ex
        m.submodules.ma = self.ma
        m.submodules.wb = self.wb

        # IF
        m.d.comb += self.pc.increment.eq(self.enable)
        m.d.comb += self.iaddr.eq(Mux(self.pc.set, self.pc.a, self.pc.q))

        # ID
        m.d.comb += self.id.cond.eq(self.ex.cond_out)
        m.d.comb += self.id.instr.eq(self.idata)
        with m.If(self.enable):
            m.d.sync += self.id.skip.eq(G(self.id.ucode, "no_delay"))

        # microcode sequence
        with m.If(self.enable):
            m.d.sync += self.ex.ucode.eq(self.id.ucode)
            m.d.sync += self.ma.ucode.eq(self.ex.ucode)
            m.d.sync += self.wb.ucode.eq(self.ma.ucode)

        # EX
        with m.If(self.enable):
            m.d.sync += self.ex.pc_p4.eq(self.pc.q)
            m.d.sync += self.ex.imm.eq(self.id.imm)
            m.d.sync += self.ex.cond_in.eq(self.ex.cond_out)

        # operand bypass
        m.d.comb += self.ex.rd_ma.eq(G(self.ma.ucode, "rd"))
        m.d.comb += self.ex.rd_wb.eq(G(self.wb.ucode, "rd"))
        m.d.comb += self.ex.a_ma.eq(self.ma.a)
        m.d.comb += self.ex.a_wb.eq(self.wb.bypass_a)
        with m.If(self.enable):
            m.d.sync += self.ex.rd_wb1.eq(self.ex.rd_wb)
            m.d.sync += self.ex.a_wb1.eq(self.ex.a_wb)

        # MA
        with m.If(self.enable):
            m.d.sync += self.ma.a.eq(self.ex.q)
            m.d.sync += self.ma.vt.eq(self.ex.vt)

        # WB
        with m.If(self.enable):
            m.d.sync += self.wb.a.eq(self.ma.q)

        # memory
        m.d.comb += self.daddr.eq(self.ma.daddr)
        m.d.comb += self.dout.eq(self.ma.dout)
        m.d.comb += self.dsel.eq(self.ma.dsel)
        m.d.comb += self.dread.eq(self.ma.dread)
        m.d.comb += self.dwrite.eq(self.ma.dwrite)
        m.d.comb += self.wb.din.eq(self.din)

        return m
