"""Microcode definitions."""

from amaranth import Signal

from ..lib.opcodes import Opcodes


__all__ = [
    "ComparisonType",
    "G",
    "ImmType",
    "MEM_TBL",
    "Microcode",
    "URom",
]


MEM_TBL = [
    (0b10000000, 56, 64),
    (0b01000000, 48, 56),
    (0b00100000, 40, 48),
    (0b00010000, 32, 40),
    (0b00001000, 24, 32),
    (0b00000100, 16, 24),
    (0b00000010, 8, 16),
    (0b00000001, 0, 8),

    (0b11000000, 48, 64),
    None,
    (0b00110000, 32, 48),
    None,
    (0b00001100, 16, 32),
    None,
    (0b00000011, 0, 16),
    None,

    (0b11110000, 32, 64),
    None,
    None,
    None,
    (0b00001111, 0, 32),
    None,
    None,
    None,

    (0b11111111, 0, 64),
    None,
    None,
    None,
    None,
    None,
    None,
    None,
]


class ImmType:
    """Instruction immediate type."""

    NONE = 0
    I = 2
    S = 3
    L = 4
    X = 5
    B = 6


class ComparisonType:
    """ALU comparison type."""

    NONE = 0b00000
    EQZ = 0b10010
    NEZ = 0b10011
    LT = 0b11000
    GE = 0b11001
    LTU = 0b11100
    GEU = 0b11101
    LE = 0b11010
    GT = 0b11011
    LEU = 0b11110
    GTU = 0b11111


class MicrocodeDef:
    def __init__(self):
        self.bits = 0
        self.fields = {}

    def field(self, name, length=1):
        self.fields[name] = self.bits, length
        self.bits += length

    def make(self, **kwargs):
        n = 0
        for k, v in kwargs.items():
            n |= int(v) << self.fields[k][0]
        return n


udef = MicrocodeDef()
# Register operands.
udef.field("rd", 6)
udef.field("rs", 6)
udef.field("rt", 6)
# Whether the instruction is invalid.
udef.field("invalid")
# Whether each operand is present in the instruction.
udef.field("has_rd")
udef.field("has_rs")
udef.field("has_rt")
# Override rs with the program counter.
udef.field("rs_pc")
# Override rt with the immediate.
udef.field("rt_imm")
# Output address+4 or address+8 directly from EX.
udef.field("pc4")
udef.field("pc8")
# Immediate type, if present.
udef.field("imm", 3)
# Shift immediate by 2.
udef.field("imm_shift")
# Branch to ALU output.
udef.field("branch")
# Cancel delay slot.
udef.field("no_delay")
# ALU operation.
udef.field("alu_op", 7)
# ALU comparison.
udef.field("compare", 5)
# Condition code set, negate and signed.
udef.field("movc", 3)
# Memory read and write.
udef.field("mread")
udef.field("mwrite")
# Memory operand size and signedness.
udef.field("msize", 3)

uinv = udef.make(invalid=1)


def G(signal, field):
    """Get the specified field in a microcode signal."""
    b, length = udef.fields[field]
    return signal[b:b + length]


def Microcode(*args, **kwargs):
    """Create a microcode line signal."""
    return Signal(udef.bits, *args, **kwargs)


class URom:
    """Microcode ROM."""

    sys = [uinv] * 64
    major = [uinv] * 64
    minor = {0: [uinv] * 64}

    @staticmethod
    def make(op, **kwargs):
        """Generate the microcode entry for an opcode."""
        line = udef.make(**kwargs)
        if op.sys:
            URom.sys[op.major] = line
        else:
            if op.minor is None:
                URom.major[op.major] = line
            else:
                URom.minor[op.major][op.minor] = line


URom.make(
    Opcodes.ADD,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.ADDI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.AND,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0001000,
)

URom.make(
    Opcodes.ANDI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0001000,
)

URom.make(
    Opcodes.ANDN,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0000010,
)

URom.make(
    Opcodes.B,
    rs_pc=1, rt_imm=1,
    imm=ImmType.B, imm_shift=1,
    branch=1,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.BL,
    has_rd=1, rs_pc=1, rt_imm=1, pc8=1,
    imm=ImmType.L, imm_shift=1,
    branch=1,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.BLX,
    has_rd=1, rs_pc=1, rt_imm=1, pc4=1,
    imm=ImmType.L, imm_shift=1,
    branch=1, no_delay=1,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.BSWAPD,
    has_rd=1, has_rs=1,
    alu_op=0b1011010,
)

URom.make(
    Opcodes.BSWAPQ,
    has_rd=1, has_rs=1,
    alu_op=0b1011011,
)

URom.make(
    Opcodes.BSWAPW,
    has_rd=1, has_rs=1,
    alu_op=0b1011001,
)

URom.make(
    Opcodes.BX,
    rs_pc=1, rt_imm=1,
    imm=ImmType.B, imm_shift=1,
    branch=1, no_delay=1,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.CEQ,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0000110, compare=ComparisonType.EQZ,
)

URom.make(
    Opcodes.CEQI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0000110, compare=ComparisonType.EQZ,
)

URom.make(
    Opcodes.CGE,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0110011, compare=ComparisonType.GE,
)

URom.make(
    Opcodes.CGEI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.GE,
)

URom.make(
    Opcodes.CGEU,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0110011, compare=ComparisonType.GEU,
)

URom.make(
    Opcodes.CGEUI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.GEU,
)

URom.make(
    Opcodes.CGTI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.GT,
)

URom.make(
    Opcodes.CGTUI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.GTU,
)

URom.make(
    Opcodes.CLEI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.LE,
)

URom.make(
    Opcodes.CLEUI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.LEU,
)

URom.make(
    Opcodes.CLO,
    has_rd=1, has_rs=1,
    alu_op=0b1100110,
)

URom.make(
    Opcodes.CLT,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0110011, compare=ComparisonType.LT,
)

URom.make(
    Opcodes.CLTI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.LT,
)

URom.make(
    Opcodes.CLTU,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0110011, compare=ComparisonType.LTU,
)

URom.make(
    Opcodes.CLTUI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0110011, compare=ComparisonType.LTU,
)

URom.make(
    Opcodes.CLZ,
    has_rd=1, has_rs=1,
    alu_op=0b1100010,
)

URom.make(
    Opcodes.CNE,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0000110, compare=ComparisonType.NEZ,
)

URom.make(
    Opcodes.CNEI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0000110, compare=ComparisonType.NEZ,
)

URom.make(
    Opcodes.CNTC,
    has_rd=1, has_rs=1,
    alu_op=0b1100100,
)

URom.make(
    Opcodes.CNTEST,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0001000, compare=ComparisonType.EQZ,
)

URom.make(
    Opcodes.CNTESTI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0001000, compare=ComparisonType.EQZ,
)

URom.make(
    Opcodes.CNTS,
    has_rd=1, has_rs=1,
    alu_op=0b1100000,
)

URom.make(
    Opcodes.CTEST,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0001000, compare=ComparisonType.NEZ,
)

URom.make(
    Opcodes.CTESTI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0001000, compare=ComparisonType.NEZ,
)

URom.make(
    Opcodes.CTO,
    has_rd=1, has_rs=1,
    alu_op=0b1100101,
)

URom.make(
    Opcodes.CTZ,
    has_rd=1, has_rs=1,
    alu_op=0b1100001,
)

URom.make(
    Opcodes.EXTB,
    has_rd=1, has_rs=1,
    alu_op=0b1010100,
)

URom.make(
    Opcodes.EXTBU,
    has_rd=1, has_rs=1,
    alu_op=0b1010000,
)

URom.make(
    Opcodes.EXTW,
    has_rd=1, has_rs=1,
    alu_op=0b1010101,
)

URom.make(
    Opcodes.EXTWU,
    has_rd=1, has_rs=1,
    alu_op=0b1010001,
)

URom.make(
    Opcodes.EXTD,
    has_rd=1, has_rs=1,
    alu_op=0b1010110,
)

URom.make(
    Opcodes.EXTDU,
    has_rd=1, has_rs=1,
    alu_op=0b1010010,
)

URom.make(
    Opcodes.JLR,
    has_rd=1, has_rs=1, pc8=1,
    branch=1,
    alu_op=0b0001010,
)

URom.make(
    Opcodes.JLRX,
    has_rd=1, has_rs=1, pc4=1,
    branch=1, no_delay=1,
    alu_op=0b0001010,
)

URom.make(
    Opcodes.LAPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
)

URom.make(
    Opcodes.LB,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b100,
)

URom.make(
    Opcodes.LBPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b100,
)

URom.make(
    Opcodes.LBU,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b000,
)

URom.make(
    Opcodes.LBUPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b000,
)

URom.make(
    Opcodes.LD,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b110,
)

URom.make(
    Opcodes.LDPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b110,
)

URom.make(
    Opcodes.LDU,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b010,
)

URom.make(
    Opcodes.LDUPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b010,
)

URom.make(
    Opcodes.LQ,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b011,
)

URom.make(
    Opcodes.LQPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b011,
)

URom.make(
    Opcodes.LW,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b101,
)

URom.make(
    Opcodes.LWPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b101,
)

URom.make(
    Opcodes.LWU,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0101100,
    mread=1, msize=0b001,
)

URom.make(
    Opcodes.LWUPC,
    has_rd=1, rs_pc=1, rt_imm=1,
    imm=ImmType.L,
    alu_op=0b0101100,
    mread=1, msize=0b001,
)

URom.make(
    Opcodes.MOVC,
    has_rd=1,
    movc=0b110,
)

URom.make(
    Opcodes.MOVCU,
    has_rd=1,
    movc=0b100,
)

URom.make(
    Opcodes.MOVNC,
    has_rd=1,
    movc=0b111,
)

URom.make(
    Opcodes.MOVNCU,
    has_rd=1,
    movc=0b101,
)

URom.make(
    Opcodes.OR,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0001110,
)

URom.make(
    Opcodes.ORI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0001110,
)

URom.make(
    Opcodes.ORN,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0001011,
)

URom.make(
    Opcodes.SB,
    has_rs=1, has_rt=1, rt_imm=1,
    imm=ImmType.S,
    alu_op=0b0101100,
    mwrite=1, msize=0b000,
)

URom.make(
    Opcodes.SBPC,
    has_rt=1, rs_pc=1, rt_imm=1,
    imm=ImmType.X,
    alu_op=0b0101100,
    mwrite=1, msize=0b000,
)

URom.make(
    Opcodes.SD,
    has_rs=1, has_rt=1, rt_imm=1,
    imm=ImmType.S,
    alu_op=0b0101100,
    mwrite=1, msize=0b010,
)

URom.make(
    Opcodes.SDPC,
    has_rt=1, rs_pc=1, rt_imm=1,
    imm=ImmType.X,
    alu_op=0b0101100,
    mwrite=1, msize=0b010,
)

URom.make(
    Opcodes.SHL,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b1001001,
)

URom.make(
    Opcodes.SHLI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b1001001,
)

URom.make(
    Opcodes.SHR,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b1001010,
)

URom.make(
    Opcodes.SHRI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b1001010,
)

URom.make(
    Opcodes.SHRU,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b1001011,
)

URom.make(
    Opcodes.SHRUI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b1001011,
)

URom.make(
    Opcodes.SQ,
    has_rs=1, has_rt=1, rt_imm=1,
    imm=ImmType.S,
    alu_op=0b0101100,
    mwrite=1, msize=0b011,
)

URom.make(
    Opcodes.SQPC,
    has_rt=1, rs_pc=1, rt_imm=1,
    imm=ImmType.X,
    alu_op=0b0101100,
    mwrite=1, msize=0b011,
)

URom.make(
    Opcodes.SUB,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0110011,
)

URom.make(
    Opcodes.SW,
    has_rs=1, has_rt=1, rt_imm=1,
    imm=ImmType.S,
    alu_op=0b0101100,
    mwrite=1, msize=0b001,
)

URom.make(
    Opcodes.SWPC,
    has_rt=1, rs_pc=1, rt_imm=1,
    imm=ImmType.X,
    alu_op=0b0101100,
    mwrite=1, msize=0b001,
)

URom.make(
    Opcodes.XOR,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0000110,
)

URom.make(
    Opcodes.XORI,
    has_rd=1, has_rs=1, rt_imm=1,
    imm=ImmType.I,
    alu_op=0b0000110,
)

URom.make(
    Opcodes.XORN,
    has_rd=1, has_rs=1, has_rt=1,
    alu_op=0b0001001,
)
