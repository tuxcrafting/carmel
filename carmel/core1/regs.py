"""Register modules."""

from amaranth import Array, Cat, Elaboratable, Module, Mux, Signal


__all__ = [
    "Counter",
    "RegisterBank",
]


class Counter(Elaboratable):
    """Counter, can increment by a constant or be set.

    Parameters
    ----------
    width : int
        Width of the counter.
    inc_by : int
        Value to increment by.
    default : int
        Default value.

    Attributes
    ----------
    increment : Signal(), in
        Increment the counter at the clock edge.
    set : Signal(), in
        Set the value.
    a : Signal(width), in
        Value to set.
    q : Signal(width), out
        Value of the counter.

    """

    def __init__(self, width, inc_by=1, default=0):  # noqa: D107
        self.width = width
        self.inc_by = inc_by
        self.default = default

        self.increment = Signal()
        self.set = Signal()
        self.a = Signal(self.width)
        self.q = Signal(self.width, reset=default)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        v = Mux(self.set, self.a, self.q)
        m.d.sync += self.q.eq(Mux(self.increment, v + self.inc_by, v))

        return m


class RegisterBank(Elaboratable):
    """Multi-ported register bank, ignores register 0.

    Parameters
    ----------
    width : int
        Width of each register.
    select : int
        Width of the selector.
    read_n : int
        Number of read ports.
    write_n : int
        Number of write ports.

    Attributes
    ----------
    read[read_n] : ReadPort(width, select)
        Read ports.
    write[write_n] : WritePort(width, select)
        Write ports.

    """

    class ReadPort:
        """Register bank synchronous read port.

        Parameters
        ----------
        width : int
            Width of each register.
        select : int
            Width of the selector.

        Attributes
        ----------
        sel : Signal(select), in
            Register selector.
        q : Signal(width), out
            Register value.

        """

        def __init__(self, width, select):
            self.width = width
            self.select = select

            self.sel = Signal(self.select)
            self.q = Signal(self.width)

    class WritePort:
        """Register bank synchronous write port.

        Parameters
        ----------
        width : int
            Width of each register.
        select : int
            Width of the selector.

        Attributes
        ----------
        enable : Signal(), in
            Whether to actually write.
        sel : Signal(select), in
            Register selector.
        a : Signal(width), in
            Register value.

        """

        def __init__(self, width, select):
            self.width = width
            self.select = select

            self.enable = Signal()
            self.sel = Signal(self.select)
            self.a = Signal(self.width)

    def __init__(self, width, select, read_n, write_n):  # noqa: D107
        self.width = width
        self.select = select
        self.read_n = read_n
        self.write_n = write_n

        self.regs = 2 ** self.select

        self.data = Array(
            Signal(self.width * (i != 0), name=f"data_{i}")
            for i in range(self.regs))

        self.read = [
            RegisterBank.ReadPort(self.width, self.select)
            for _ in range(self.read_n)]
        self.write = [
            RegisterBank.WritePort(self.width, self.select)
            for _ in range(self.write_n)]

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        for read in self.read:
            m.d.sync += read.q.eq(Mux(read.sel != 0, self.data[read.sel], 0))

        for write in self.write:
            with m.If(write.enable & (write.sel != 0)):
                m.d.sync += self.data[write.sel].eq(write.a)

        return m
