"""Writeback stage."""

from amaranth import Cat, Elaboratable, Module, Mux, Signal

from .alu import BitExtender
from .microcode import G, MEM_TBL, Microcode


__all__ = [
    "Writeback",
]


class Writeback(Elaboratable):
    """Pipeline writeback stage.

    Parameters
    ----------
    rd : RegisterBank.WritePort(64, 6)
        Register write port.

    Attributes
    ----------
    ucode : Microcode(), in
        Microcode line.
    a : Signal(64), in
        Input from MA.
    bypass_a : Signal(64), out
        Output operand for bypass.
    din : Signal(64), in
        Data cache connection.

    """

    def __init__(self, rd):  # noqa: D107
        self.rd = rd

        self.ucode = Microcode(name="ucode")
        self.a = Signal(64)
        self.bypass_a = Signal(64)
        self.din = Signal(64)

        self.ext = BitExtender(3, 6)

    def elaborate(self, _platform):  # noqa: D102
        m = Module()

        m.submodules.ext = self.ext

        with m.Switch(Cat(self.a[:3], G(self.ucode, "msize")[:2])):
            for i in range(32):
                with m.Case(i):
                    t = MEM_TBL[i]
                    if t is None:
                        pass
                    else:
                        _dsel, a, b = t
                        m.d.comb += self.ext.a.eq(self.din[a:b])

        m.d.comb += self.ext.size.eq(G(self.ucode, "msize")[:2])
        m.d.comb += self.ext.signed.eq(G(self.ucode, "msize")[2])
        vd = Mux(G(self.ucode, "mread"), self.ext.q, self.a)

        m.d.comb += self.rd.enable.eq(1)
        m.d.comb += self.rd.sel.eq(G(self.ucode, "rd"))
        m.d.comb += self.rd.a.eq(vd)
        m.d.comb += self.bypass_a.eq(vd)

        return m
