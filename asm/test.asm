; Carmel core1 test code.
;
; Uses a special communication port at address -8.
; Commands are:
; - 1 - Stop execution.
; - 2 - Indicate failure.

include "../inc/fasmg-stuff/utility/@@.inc"
include "../inc/carmel.inc"

  org 3 shl 62

main:
  movi #4, k0

  lapc (w0), v0
  lapc (w1), v1

  ; test memory and extension
iterate s, b, w, d
  l#s (v0), t0
  l#s#u (v0), t2
  l#s#pc (w0), t1
  l#s#upc (w0), t3
  ceq t0, t1, null
  bx.nc fail
  ceq t2, t3, null
  bx.nc fail

  ext#s t0, t0
  ext#s#u t2, t2
  ceq t0, t1, null
  bx.nc fail
  ceq t2, t3, null
  bx.nc fail

  l#s (v1), t0
  l#s#u (v1), t2
  l#s#pc (w1), t1
  l#s#upc (w1), t3
  ceq t0, t1, null
  bx.nc fail
  ceq t2, t3, null
  bx.nc fail

  ext#s t0, t0
  ext#s#u t2, t2
  ceq t0, t1, null
  bx.nc fail
  ceq t2, t3, null
  bx.nc fail
end iterate

  ; byte swapping
iterate <ls,bs,o>, wu,w,6, du,d,4, q,q,0
  l#ls#pc (w0 + 0), t0
  l#ls#pc (w1 + o), t1
  bswap#bs t0, t0
  ceq t0, t1, null
  bx.nc fail
end iterate

  ; bit manipulation
  movi #0x342, t0
  movi #-1, t2

  cntc t0, t1
  ceqi t1, #60, null
  bx.nc fail
  cnts t0, t1
  ceqi t1, #4, null
  bx.nc fail

  ctz null, t1
  ceqi t1, #64, null
  bx.nc fail
  ctz t2, t1
  ceqi t1, #0, null
  bx.nc fail
  cto null, t1
  ceqi t1, #0, null
  bx.nc fail
  cto t2, t1
  ceqi t1, #64, null
  bx.nc fail
  clz null, t1
  ceqi t1, #64, null
  bx.nc fail
  clz t2, t1
  ceqi t1, #0, null
  bx.nc fail
  clo null, t1
  ceqi t1, #0, null
  bx.nc fail
  clo t2, t1
  ceqi t1, #64, null
  bx.nc fail

  ; bitwise ops
  movi #1010b, t0
  movi #1100b, t1
  and t0, t1, t2
  ceqi t2, #1000b, null
  bx.nc fail
  andn t0, t1, t2
  ceqi t2, #0010b, null
  bx.nc fail
  andi t0, #1100b, t2
  ceqi t2, #1000b, null
  bx.nc fail
  or t0, t1, t2
  ceqi t2, #1110b, null
  bx.nc fail
  orn t0, t1, t2
  ceqi t2, #(-1 shl 4) or 1011b, null
  bx.nc fail
  ori t0, #1100b, t2
  ceqi t2, #1110b, null
  bx.nc fail
  xor t0, t1, t2
  ceqi t2, #0110b, null
  bx.nc fail
  xori t0, #1100b, t2
  ceqi t2, #0110b, null
  bx.nc fail
  xorn t0, t1, t2
  ceqi t2, #(-1 shl 4) or 1001b, null
  bx.nc fail

  ; arithmetic
  movi #141, t0
  movi #33, t1
  add t0, t1, t2
  ceqi t2, #141 + 33, null
  bx.nc fail
  sub t0, t1, t2
  ceqi t2, #141 - 33, null
  bx.nc fail
  addi t0, #33, t2
  ceqi t2, #141 + 33, null
  bx.nc fail

  ; shifter
  movi #-16, t0
  movi #3, t1
  shl t0, t1, t2
  ceqi t2, #-128, null
  bx.nc fail
  shr t0, t1, t2
  ceqi t2, #-2, null
  bx.nc fail
  lqpc (w2), t3
  shru t0, t1, t2
  ceq t2, t3, null
  bx.nc fail

  ; condition code
  ceq null, null, t0
  ceqi t0, #1, null
  bx.nc fail
  ceqi null, #1, t0
  ceqi t0, #0, null
  bx.nc fail

  ceq null, null, null
  movnc t0
  ceqi t0, #0, null
  bx.nc fail
  ceq null, null, null
  movncu t0
  ceqi t0, #0, null
  bx.nc fail
  ceq null, null, null
  movc t0
  ceqi t0, #-1, null
  bx.nc fail
  ceq null, null, null
  movcu t0
  ceqi t0, #1, null
  bx.nc fail

  cne null, null, null
  movnc t0
  ceqi t0, #-1, null
  bx.nc fail
  cne null, null, null
  movncu t0
  ceqi t0, #1, null
  bx.nc fail
  cne null, null, null
  movc t0
  ceqi t0, #0, null
  bx.nc fail
  cne null, null, null
  movcu t0
  ceqi t0, #0, null
  bx.nc fail

  ; comparisons
  movi #14, t0
  movi #-3, t1

  ceq t0, t0, null
  bx.nc fail
  cne t0, t0, null
  bx.c fail
  ceq t0, t1, null
  bx.c fail
  cne t0, t1, null
  bx.nc fail

  clt t0, t0, null
  bx.c fail
  clt t0, t1, null
  bx.c fail
  clt t1, t0, null
  bx.nc fail

  cltu t0, t0, null
  bx.c fail
  cltu t0, t1, null
  bx.nc fail
  cltu t1, t0, null
  bx.c fail

  cge t0, t0, null
  bx.nc fail
  cge t0, t1, null
  bx.nc fail
  cge t1, t0, null
  bx.c fail

  cgeu t0, t0, null
  bx.nc fail
  cgeu t0, t1, null
  bx.c fail
  cgeu t1, t0, null
  bx.nc fail

  ctesti t0, #1000, null
  bx.nc fail
  ctesti t0, #0001, null
  bx.c fail
  cntesti t0, #1000, null
  bx.c fail
  cntesti t0, #0001, null
  bx.nc fail

  cgti t0, #4, null
  bx.nc fail
  clei t0, #14, null
  bx.nc fail

  sq k0, -8(null)

align 8
w0: dq 0x0123456789ABCDEF
w1: dq 0xEFCDAB8967452301
w2: dq 0x1FFFFFFFFFFFFFFE

puts:
  lbu (a0), a1
  addi a0, #1, a0
  ceqi a1, #0, null
  jlrx.c ra, null
  b puts
  sq a1, -8(null)

fail:
  bl puts, ra
  lapc (fail_msg), a0
  sq k0, -8(null)
@@:
  b @b

fail_msg: db "Fail", 10, 4, 0
