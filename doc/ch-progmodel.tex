\chapter{Programming model}

\section{Registers}

The Carmel ISA has 32 general purpose registers (GPRs), labeled \textsf{R0} to
\textsf{R31}, one program counter \textsf{PC} (which holds the address of the
next instruction), and a single bit flag \textsf{C}. The GPRs all hold 4 64-bit
fields along with a 1-bit tag, specifying if the other fields indicate a valid
capability. \textsf{PC} always holds a valid executable capability to the next
instruction.

Register \textsf{R0} is always all-clear and writes to it are ignored. Other
GPRs have no architectural meaning, but are assigned specific ABI meanings:

\begin{table}[H]
  \centering
  \caption{ABI mapping of GPRs}
  \begin{tabular}{rrl}
    \toprule
    ABI name & Register & Description \\
    \midrule
    \textsf{NULL} & \textsf{R0} & Always 0 \\
    \textsf{RA} & \textsf{R1} & Procedure return address \\
    \textsf{GP} & \textsf{R2} & Global pointer \\
    \textsf{TP} & \textsf{R3} & Thread pointer \\
    \textsf{SP} & \textsf{R4} & Stack pointer \\
    \textsf{FP} & \textsf{R5} & Frame pointer \\
    \textsf{K0}-\textsf{K1} & \textsf{R6}-\textsf{R7} & Reserved for system use \\
    \textsf{V0}-\textsf{V11} & \textsf{R8}-\textsf{R19} & Callee-saved function variables \\
    \textsf{A0}-\textsf{A11} & \textsf{R20}-\textsf{R31} & Caller-saved argument and return, or temporary values \\
    \bottomrule
  \end{tabular}
\end{table}

\subsection{Initial state}

At start, every GPR except \textsf{R30} and \textsf{R31} is filled with an all-clear pattern, and
\textsf{C} is similarly clear. \textsf{PC}, \textsf{R30} and \textsf{R31}
contain the following capabilities:

\begin{description}
\item[\textsf{PC}]
  \begin{align*}
    B_U & = FFFFFFFFFFFFFFFF_{16} \\
    B_L & = 0000000000000000_{16} \\
    O & = FFFFFFFF_{16} \\
    U & = FFFF_{16} \\
    P_I & = 1 \\
    P_{EU} & = 0 \\
    P_{ES} & = 0 \\
    P_X & = 1 \\
    P_{SLC} & = 0 \\
    P_{SC} & = 0 \\
    P_{LC} & = 1 \\
    P_S & = 0 \\
    P_L & = 1 \\
    P_G & = 1 \\
    A & = 0000000000000000_{16} \\
  \end{align*}
\item[\textsf{R30}]
  \begin{align*}
    B_U & = 00000000FFFFFEFF_{16} \\
    B_L & = 0000000000000000_{16} \\
    O & = FFFFFFFF_{16} \\
    U & = FFFF_{16} \\
    P_I & = 0 \\
    P_{EU} & = 1 \\
    P_{ES} & = 1 \\
    P_X & = 0 \\
    P_{SLC} & = 0 \\
    P_{SC} & = 0 \\
    P_{LC} & = 0 \\
    P_S & = 0 \\
    P_L & = 0 \\
    P_G & = 1 \\
    A & = 0000000000000000_{16} \\
  \end{align*}
\item[\textsf{R31}]
  \begin{align*}
    B_U & = FFFFFFFFFFFFFFFF_{16} \\
    B_L & = 0000000000000000_{16} \\
    O & = FFFFFFFF_{16} \\
    U & = FFFF_{16} \\
    P_I & = 0 \\
    P_{EU} & = 0 \\
    P_{ES} & = 0 \\
    P_X & = 0 \\
    P_{SLC} & = 1 \\
    P_{SC} & = 1 \\
    P_{LC} & = 1 \\
    P_S & = 1 \\
    P_L & = 1 \\
    P_G & = 1 \\
    A & = 0000000000000000_{16} \\
  \end{align*}
\end{description}

\section{Memory}

There is conceptually one single byte-addressed address space containing
$2^{64}$ bytes. In addition, there is one tag bit for every 32 bytes, which is
used to allow storing capabilities in main memory.

This tag bit is automatically cleared whenever part of the 32-byte area is
overwritten by a scalar value, to avoid being able to forge unauthorized
capabilities.

Carmel is an exclusively big-endian architecture (more significant bytes come
first in memory), however there are several instructions provided to swap
endianness.

\section{Data format}

Each register holds a 257-bit value:

\begin{table}[H]
  \centering
  \caption{Register format}
  \vspace{\smallskipamount}
  \begin{bytefield}[bitwidth=\bitwidthSF]{64}
    \bitheader{0,15,16,31,32,63}
    \\
    \begin{leftwordgroup}{Tag $T$}
      \bitbox{63}[bgcolor=lightgray]{}
      \bitbox{1}{}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Upper bound}
      \bitbox{64}{$B_U[63:0]$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Lower bound}
      \bitbox{64}{$B_L[63:0]$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Flags}
      \bitbox{32}{$O[31:0]$}
      \bitbox{16}{$U[15:0]$}
      \bitbox{16}{$P[15:0]$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Address}
      \bitbox{64}{$A[63:0]$}
    \end{leftwordgroup}
  \end{bytefield}
\end{table}

\begin{description}
\item[Tag $T$] If set, then the value is a valid capability. Else, $A$ holds a
  scalar, and the other fields don't have any special meaning (they are still
  able to be extracted and modified, though).
\item[$B_U$, $B_L$] Inclusive bounds of the capability address. If $B_U$ and
  $B_L$ are equal, the capability has a range of precisely one address, which is
  the minimum. Bounds may not wrap around the edge of the address space.
\item[$O$] Object type. The following types are specified:
  \begin{table}[H]
    \centering
    \caption{Object types $O$}
    \begin{tabular}{rl}
      \toprule
      Value & Description \\
      \midrule
      $FFFFFFFF_{16}$ & Unsealed capability. \\
      $FFFFFFFE_{16}$ & Sealed entry capability. \\
      $00000000_{16}$-$FFFFFEFF_{16}$ & Sealed object capability. \\
      \bottomrule
    \end{tabular}
  \end{table}
\item[$U$] User-level permission bitmap.
\item[$P$] Architectural permission bitmap.
\item[$A$] Capability address, or a 64-bit scalar value.
\end{description}

\begin{table}[H]
  \centering
  \caption{Capability permission bitmap $P$}
  \vspace{\smallskipamount}
  \begin{bytefield}[bitwidth=\bitwidthE]{8}
    \bitheader{0-7}
    \\
    \bitbox{6}[bgcolor=lightgray]{}
    \bitbox{1}{$P_I$}
    \bitbox{1}{$P_{OU}$}
    \\
    \bitbox{1}{$P_{OS}$}
    \bitbox{1}{$P_X$}
    \bitbox{1}{$P_{SLC}$}
    \bitbox{1}{$P_{SC}$}
    \bitbox{1}{$P_{LC}$}
    \bitbox{1}{$P_S$}
    \bitbox{1}{$P_L$}
    \bitbox{1}{$P_G$}
  \end{bytefield}
\end{table}

\begin{description}
\item[$P_I$] Allow using \instrref{INVK} on the capability.
\item[$P_{EU}$] Allow unsealing capabilities with an $O$ equal to the unsealing
  capability's $A$, that is within bounds.
\item[$P_{ES}$] Allow sealing capabilities with an $O$ equal to the unsealing
  capability's $A$, that is within bounds.
\item[$P_X$] Allow executing instructions from this capability.
\item[$P_{SLC}$] Allow storing capabilities without $P_G$ in this capability.
  Implies $P_{SC}$ and $P_S$.
\item[$P_{SC}$] Allow storing capabilities with $P_G$ in this capability.
  Implies $P_S$.
\item[$P_{LC}$] Allow loading capabilities from this capability.
\item[$P_S$] Allow storing scalar data in this capability.
\item[$P_L$] Allow loading scalar data from this capability.
\item[$P_G$] Flag indicating whether the capability is global (if set), else
  local.
\end{description}

\section{Capabilities}

\subsection{Bounds checking}

The address $A$ of a capability must be within its bounds $B_L$ to $B_U$
(inclusive, no wrapping) when it is used. The address may be outside of the
bounds when the capability is being purely manipulated, but an out-of-bounds
memory access or (un)sealing triggers an exception.

\subsection{Sealing}

A capability can be sealed: this way, it cannot be modified. Sealing is done by
using a capability with the seal permission, and it can be unsealed with a
capability with the unseal permission matching its $O$.

There is also a special type of sealed capability, the sealed entry capability.
Like a sealed capability, it cannot be used directly for memory access, however,
it can be jumped to like any normal capability. Sealed entry capabilities are
used for return addresses, for example.
