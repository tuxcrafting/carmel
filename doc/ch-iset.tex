\chapter{Instruction set}

\section{Encoding}

Instructions all fit in an aligned doubleword, They can have one of several
formats:

\begin{table}[h]
  \centering
  \caption{Instruction formats}
  \vspace{\smallskipamount}
  \begin{bytefield}[bitwidth=\bitwidthTT]{32}
    \bitheader{0,1,2,6,7,9,10,14,15,17,18,19,20,24,25,31}
    \\
    \begin{leftwordgroup}{Register}
      \bitbox{7}{$major$}
      \bitbox{5}{$rd$}
      \bitbox{5}{$rs$}
      \bitbox{5}{$rt$}
      \bitbox{3}[bgcolor=lightgray]{}
      \bitbox{7}{$minor$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Immediate}
      \bitbox{7}{$major$}
      \bitbox{5}{$rd$}
      \bitbox{5}{$rs$}
      \bitbox{15}{$imm[14:0]$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Store}
      \bitbox{7}{$major$}
      \bitbox{5}{$I$}
      \bitbox{5}{$rs$}
      \bitbox{5}{$rt$}
      \bitbox{10}{$imm[9:0]$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{PC-relative}
      \bitbox{7}{$major$}
      \bitbox{5}{$rd$}
      \bitbox{20}{$imm[19:0]$}
    \end{leftwordgroup}
    \\
    \begin{leftwordgroup}{Branch}
      \bitbox{7}{$major$}
      \bitbox{5}{$rd$}
      \bitbox{18}{$imm[19:2]$}
      \bitbox{2}{$J$}
    \end{leftwordgroup}
  \end{bytefield}
\end{table}

\begin{description}[style=nextline]
\item[$major$] Major operation code.
\item[$minor$] Minor operation code.
\item[$rd$, $rs$, $rt$] Register operands.
\item[$I$] Alias of $imm[14:10]$.
\item[$J$] Alias of $imm[21:20]$.
\item[$imm$] Sign-extended immediate operand.
\end{description}

\section{Instruction reference}

\subsection{Pseudocode}

Instruction behavior is also specified in pseudocode. There are a few helper
functions used:

\begin{algorithm}[H]
  \tcc{Set the scalar value of a register, clearing the upper bits.}
  \Fn{$SetScalar(dest, value)$}{
    $dest[256:64] \gets 0$\;
    $dest.A \gets value$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Set the address in a capability, copying the upper bits unchanged.}
  \Fn{$SetAddress(dest, base, value)$}{
    $dest[256:64] \gets base[256:64]$\;
    $dest.A \gets value$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Logical AND the permissions (user and architectural) of a capability with
  a mask.}
  \Fn{$AndPerm(dest, base, mask)$}{
    $dest[256:96] \gets base[256:96]$\;
    $dest[95:64] \gets base[95:64] \And mask$\;
    $dest[63:0] \gets base[63:0]$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Set \textsf{C} and a register to the result of a comparison.}
  \Fn{$SetCondition(dest, cond)$}{
    \eIf{$cond$}{
      $\textsf{C} \gets 1$\;
    }{
      $\textsf{C} \gets 0$\;
    }
    $SetScalar(dest, \textsf{C})$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Check if a capability is within bounds and accessible.}
  \Fn{$CheckAccess(cap, mask, perms)$}{
    $cond \gets perms$\;
    $cond \gets cond \land cap.O = FFFFFFFF_{16}$\;
    $cond \gets cond \land cap.A \And mask = 0$\;
    $cond \gets cond \land cap.A \geq cap.B_L$\;
    $cond \gets cond \land cap.A \Or mask \leq cap.B_U$\;
    \If{$\lnot cond$}{
      $Trap(\texttt{AccessError})$\;
    }
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Branch and link, checking bounds.}
  \Fn{$BranchLink(dest, newPC)$}{
    $dest \gets \textsf{PC}$\;
    $dest.O \gets FFFFFFFE_{16}$\;
    $CheckAccess(newPC, 3, newPC.P_X)$\;
    $\textsf{PC} \gets newPC$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Branch and link with a constant displacement.}
  \Fn{$BranchLinkDisp(dest, disp)$}{
    $newPC \gets Copy(\textsf{PC})$\;
    $newPC.A \gets newPC.A + disp$\;
    $BranchLink(dest, newPC)$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Load a scalar from memory.}
  \Fn{$LoadScalar(base, disp, size)$}{
    $addr \gets Copy(base)$\;
    $addr.A \gets addr.A + disp$\;
    $CheckAccess(addr, size - 1, addr.P_L)$\;
    $t \gets addr.A \Shl 3$\;
    $u \gets size \Shl 3$\;
    \Return{$\textsf{DataMem}[t + u - 1:t]$}\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Load a capability from memory.}
  \Fn{$LoadCapability(base, disp)$}{
    $addr \gets Copy(base)$\;
    $addr.A \gets addr.A + disp$\;
    $CheckAccess(addr, 31, addr.P_L)$\;
    $t \gets addr.A \Shl 3$\;
    $value[255:0] \gets Copy(\textsf{DataMem}[t + 255:t])$\;
    \eIf{$\lnot addr.P_{LC}$}{
      $value.T \gets 0$\;
    }{
      $value.T \gets \textsf{TagMem}[addr.A \Shr 5]$
    }
    \Return{$value$}\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Store a scalar in memory.}
  \Fn{$StoreScalar(base, disp, size, value)$}{
    $addr \gets Copy(base)$\;
    $addr.A \gets addr.A + disp$\;
    $CheckAccess(addr, size - 1, addr.P_S)$\;
    $t \gets addr.A \Shl 3$\;
    $u \gets size \Shl 3$\;
    $\textsf{DataMem}[t + u - 1:t] \gets value$\;
    $\textsf{TagMem}[addr.A \Shr 5] \gets 0$\;
  }
\end{algorithm}

\begin{algorithm}[H]
  \tcc{Store a capability in memory.}
  \Fn{$StoreCapability(base, disp, value)$}{
    $addr \gets Copy(base)$\;
    $addr.A \gets addr.A + disp$\;
    $g \gets value.P_G \lor addr.P_{SLC}$
    $CheckAccess(addr, 31, addr.P_S \land addr.P_{SC} \land g$\;
    $t \gets addr.A \Shl 3$\;
    $\textsf{DataMem}[t + 255:t] \gets value[255:0]$\;
    $\textsf{TagMem}[addr.A \Shr 5] \gets value.T$\;
  }
\end{algorithm}

\begin{instr}{ADD.C}
  \formatRegister{1111111}{0001001}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rs].A + \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Capability addition.
\end{instr}

\begin{instr}{ADD.C.I}
  \formatImmediate{0001001}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rs].A + imm)$\;
  \end{pseudo}
  \desc Capability addition.
\end{instr}

\begin{instr}{ADD.PC.I}
  \formatPCRel{1110000}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{PC}, \textsf{PC}.A + imm)$\;
  \end{pseudo}
  \desc Add immediate value to program counter.
\end{instr}

\begin{instr}{ADD.U}
  \formatRegister{1111111}{0000001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A + \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar addition.
\end{instr}

\begin{instr}{ADD.U.I}
  \formatImmediate{0000001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A + imm)$\;
  \end{pseudo}
  \desc Scalar addition.
\end{instr}

\begin{instr}{AND.C}
  \formatRegister{1111111}{0001010}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rs].A \And \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Capability logical AND.
\end{instr}

\begin{instr}{AND.C.I}
  \formatImmediate{0001010}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rs].A \And imm)$\;
  \end{pseudo}
  \desc Capability logical AND.
\end{instr}

\begin{instr}{AND.U}
  \formatRegister{1111111}{0000010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \And \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar logical AND.
\end{instr}

\begin{instr}{AND.U.I}
  \formatImmediate{0000010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \And imm)$\;
  \end{pseudo}
  \desc Scalar logical AND.
\end{instr}

\begin{instr}{ANDP.C}
  \formatRegister{1111111}{0001010}
  \begin{pseudo}
    $AndPerm(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rt].A[31:0])$\;
  \end{pseudo}
  \desc Logical AND of capability permissions.
\end{instr}

\begin{instr}{ANDP.C.I}
  \formatImmediate{0001010}
  \begin{pseudo}
    $AndPerm(\textsf{R}[rd], \textsf{R}[rs], imm[31:0])$\;
  \end{pseudo}
  \desc Logical AND of capability permissions.
\end{instr}

\begin{instr}{BL}
  \formatBranch{0100000}
  \begin{pseudo}
    $BranchLinkDisp(\textsf{R}[rd], imm)$\;
  \end{pseudo}
  \desc Branch and link.
\end{instr}

\begin{instr}{BL.CF}
  \formatBranch{0100010}
  \begin{pseudo}
    \If{$\textsf{C} = 0$}{
      $BranchLinkDisp(\textsf{R}[rd], imm)$\;
    }
  \end{pseudo}
  \desc Branch and link if \textsf{C} is false.
\end{instr}

\begin{instr}{BL.CT}
  \formatBranch{0100011}
  \begin{pseudo}
    \If{$\textsf{C} = 1$}{
      $BranchLinkDisp(\textsf{R}[rd], imm)$\;
    }
  \end{pseudo}
  \desc Branch and link if \textsf{C} is true.
\end{instr}

\begin{instr}{BLR}
  \formatRegisterRR{1111111}{0100000}
  \begin{pseudo}
    $newPC \gets Copy(\textsf{R}[rs])$\;
    \If{$newPC.O \neq FFFFFFFE_{16}$}{
      $Trap(\texttt{SealingError})$\;
    }
    $newPC.O \gets FFFFFFFF_{16}$\;
    $BranchLink(\textsf{R}[rd], newPC)$\;
  \end{pseudo}
  \desc Branch and link register. Target must be a sealed entry capability.
\end{instr}

\begin{instr}{BLR.NS}
  \formatRegisterRR{1111111}{0100001}
  \begin{pseudo}
    $newPC \gets Copy(\textsf{R}[rs])$\;
    \If{$newPC.O = FFFFFFFE_{16}$}{
      $newPC.O \gets FFFFFFFF_{16}$\;
    }
    \If{$newPC.O \neq FFFFFFFF_{16}$}{
      $Trap(\texttt{SealingError})$\;
    }
    $BranchLink(\textsf{R}[rd], newPC)$\;
  \end{pseudo}
  \desc Branch and link register. Target must not be a sealed object, but may be
  a sealed entry or access capability.
\end{instr}

\begin{instr}{BSWAP.D}
  \formatRegisterRR{1111111}{0101010}
  \begin{pseudo}
    $inp \gets \textsf{R}[rs].A$\;
    $out[7:0] \gets inp[31:24]$\;
    $out[15:8] \gets inp[23:16]$\;
    $out[23:16] \gets inp[15:8]$\;
    $out[31:24] \gets inp[7:0]$\;
    $out[63:32] \gets 0$\;
    $SetScalar(\textsf{R}[rd], out)$\;
  \end{pseudo}
  \desc Swap bytes in a doubleword.
\end{instr}

\begin{instr}{BSWAP.Q}
  \formatRegisterRR{1111111}{0101011}
  \begin{pseudo}
    $inp \gets \textsf{R}[rs].A$\;
    $out[7:0] \gets inp[63:56]$\;
    $out[15:8] \gets inp[55:48]$\;
    $out[23:16] \gets inp[47:40]$\;
    $out[31:24] \gets inp[39:32]$\;
    $out[39:32] \gets inp[31:24]$\;
    $out[47:40] \gets inp[23:16]$\;
    $out[55:48] \gets inp[15:8]$\;
    $out[63:56] \gets inp[7:0]$\;
    $SetScalar(\textsf{R}[rd], out)$\;
  \end{pseudo}
  \desc Swap bytes in a quadword.
\end{instr}

\begin{instr}{BSWAP.W}
  \formatRegisterRR{1111111}{0101001}
  \begin{pseudo}
    $inp \gets \textsf{R}[rs].A$\;
    $out[7:0] \gets inp[15:8]$\;
    $out[15:8] \gets inp[7:0]$\;
    $out[63:16] \gets 0$\;
    $SetScalar(\textsf{R}[rd], out)$\;
  \end{pseudo}
  \desc Swap bytes in a word.
\end{instr}

\begin{instr}{CMP.AND.U}
  \formatRegister{1111111}{0010010}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \And \textsf{R}[rt].A \neq 0)$\;
  \end{pseudo}
  \desc Scalar logical AND is not zero.
\end{instr}

\begin{instr}{CMP.AND.U.I}
  \formatImmediate{0010010}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \And imm \neq 0)$\;
  \end{pseudo}
  \desc Scalar logical AND is not zero.
\end{instr}

\begin{instr}{CMP.ANDP.C}
  \formatRegister{1111111}{0011010}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs][95:64] \And \textsf{R}[rt].A[31:0] \neq 0)$\;
  \end{pseudo}
  \desc Logical AND of capability permissions is not zero.
\end{instr}

\begin{instr}{CMP.ANDP.C.I}
  \formatImmediate{0011010}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs][95:64] \And imm[31:0] \neq 0)$\;
  \end{pseudo}
  \desc Logical AND of capability permissions is not zero.
\end{instr}

\begin{instr}{CMP.EQL.C}
  \formatRegister{1111111}{0011000}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs] = \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Capability equality.
\end{instr}

\begin{instr}{CMP.EQL.U}
  \formatRegister{1111111}{0010000}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A = \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar equality.
\end{instr}

\begin{instr}{CMP.EQL.U.I}
  \formatImmediate{0010000}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A = imm)$\;
  \end{pseudo}
  \desc Scalar equality.
\end{instr}

\begin{instr}{CMP.GE.S}
  \formatRegister{1111111}{0010111}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], Signed(\textsf{R}[rs].A) \geq Signed(\textsf{R}[rt].A))$\;
  \end{pseudo}
  \desc Scalar signed greater than or equal.
\end{instr}

\begin{instr}{CMP.GE.S.I}
  \formatImmediate{0010111}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], Signed(\textsf{R}[rs].A) \geq Signed(imm))$\;
  \end{pseudo}
  \desc Scalar signed greater than or equal.
\end{instr}

\begin{instr}{CMP.GE.U}
  \formatRegister{1111111}{0010101}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \geq \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar unsigned greater than or equal.
\end{instr}

\begin{instr}{CMP.GE.U.I}
  \formatImmediate{0010101}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \geq imm)$\;
  \end{pseudo}
  \desc Scalar unsigned greater than or equal.
\end{instr}

\begin{instr}{CMP.GT.S.I}
  \formatImmediate{0011110}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], Signed(\textsf{R}[rs].A) > Signed(imm))$\;
  \end{pseudo}
  \desc Scalar signed greater than.
\end{instr}

\begin{instr}{CMP.GT.U.I}
  \formatImmediate{0011100}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A > imm)$\;
  \end{pseudo}
  \desc Scalar unsigned greater than.
\end{instr}

\begin{instr}{CMP.LE.S.I}
  \formatImmediate{0011111}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], Signed(\textsf{R}[rs].A) \leq Signed(imm))$\;
  \end{pseudo}
  \desc Scalar signed less than or equal.
\end{instr}

\begin{instr}{CMP.LE.U.I}
  \formatImmediate{0011101}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \leq imm)$\;
  \end{pseudo}
  \desc Scalar unsigned less than or equal.
\end{instr}

\begin{instr}{CMP.LT.S}
  \formatRegister{1111111}{0010110}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], Signed(\textsf{R}[rs].A) < Signed(\textsf{R}[rt].A))$\;
  \end{pseudo}
  \desc Scalar signed less than.
\end{instr}

\begin{instr}{CMP.LT.S.I}
  \formatImmediate{0010110}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], Signed(\textsf{R}[rs].A) < Signed(imm))$\;
  \end{pseudo}
  \desc Scalar signed less than.
\end{instr}

\begin{instr}{CMP.LT.U}
  \formatRegister{1111111}{0010100}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A < \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar unsigned less than.
\end{instr}

\begin{instr}{CMP.LT.U.I}
  \formatImmediate{0010100}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A < imm)$\;
  \end{pseudo}
  \desc Scalar unsigned less than.
\end{instr}

\begin{instr}{CMP.NAND.U}
  \formatRegister{1111111}{0010011}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \And \textsf{R}[rt].A = 0)$\;
  \end{pseudo}
  \desc Scalar logical AND is zero.
\end{instr}

\begin{instr}{CMP.NAND.U.I}
  \formatImmediate{0010011}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \And imm = 0)$\;
  \end{pseudo}
  \desc Scalar logical AND is zero.
\end{instr}

\begin{instr}{CMP.NANDP.C}
  \formatRegister{1111111}{0011011}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs][95:64] \And \textsf{R}[rt].A[31:0] = 0)$\;
  \end{pseudo}
  \desc Logical AND of capability permissions is zero.
\end{instr}

\begin{instr}{CMP.NANDP.C.I}
  \formatImmediate{0011011}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs][95:64] \And imm[31:0] = 0)$\;
  \end{pseudo}
  \desc Logical AND of capability permissions is zero.
\end{instr}

\begin{instr}{CMP.NEQ.C}
  \formatRegister{1111111}{0011001}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs] \neq \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Capability inequality.
\end{instr}

\begin{instr}{CMP.NEQ.U}
  \formatRegister{1111111}{0010001}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \neq \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar inequality.
\end{instr}

\begin{instr}{CMP.NEQ.U.I}
  \formatImmediate{0010001}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].A \neq imm)$\;
  \end{pseudo}
  \desc Scalar inequality.
\end{instr}

\begin{instr}{CMP.NSEAL}
  \formatRegister{1111111}{0011111}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].O = FFFFFFFF_{16})$\;
  \end{pseudo}
  \desc Check if capability is not sealed.
\end{instr}

\begin{instr}{CMP.NTAG}
  \formatRegister{1111111}{0011101}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].T = 0)$\;
  \end{pseudo}
  \desc Check if capability tag is not set.
\end{instr}

\begin{instr}{CMP.SEAL}
  \formatRegister{1111111}{0011110}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].O \neq FFFFFFFF_{16})$\;
  \end{pseudo}
  \desc Check if capability is sealed.
\end{instr}

\begin{instr}{CMP.TAG}
  \formatRegister{1111111}{0011100}
  \begin{pseudo}
    $SetCondition(\textsf{R}[rd], \textsf{R}[rs].T \neq 0)$\;
  \end{pseudo}
  \desc Check if capability tag is set.
\end{instr}

\begin{instr}{EXT.BS}
  \formatRegisterRR{1111111}{0110100}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(\textsf{R}[rs].A[7:0]))$\;
  \end{pseudo}
  \desc Sign-extend byte.
\end{instr}

\begin{instr}{EXT.BU}
  \formatRegisterRR{1111111}{0110000}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A[7:0])$\;
  \end{pseudo}
  \desc Zero-extend byte.
\end{instr}

\begin{instr}{EXT.DS}
  \formatRegisterRR{1111111}{0110110}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(\textsf{R}[rs].A[31:0]))$\;
  \end{pseudo}
  \desc Sign-extend doubleword.
\end{instr}

\begin{instr}{EXT.DU}
  \formatRegisterRR{1111111}{0110010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A[31:0])$\;
  \end{pseudo}
  \desc Zero-extend doubleword.
\end{instr}

\begin{instr}{EXT.WS}
  \formatRegisterRR{1111111}{0110101}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(\textsf{R}[rs].A[15:0]))$\;
  \end{pseudo}
  \desc Sign-extend word.
\end{instr}

\begin{instr}{EXT.WU}
  \formatRegisterRR{1111111}{0110001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A[15:0])$\;
  \end{pseudo}
  \desc Zero-extend word.
\end{instr}

\begin{instr}{GET.BL.C}
  \formatRegisterRR{1111111}{0001101}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs][191:128])$\;
  \end{pseudo}
  \desc Get capability lower bound.
\end{instr}

\begin{instr}{GET.BU.C}
  \formatRegisterRR{1111111}{0001100}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs][255:192])$\;
  \end{pseudo}
  \desc Get capability upper bound.
\end{instr}

\begin{instr}{GET.F.C}
  \formatRegisterRR{1111111}{0001111}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs][127:64])$\;
  \end{pseudo}
  \desc Get capability flags.
\end{instr}

\begin{instr}{INVK}
  \formatRegister{1111111}{0100010}
  \begin{pseudo}
    $code \gets Copy(\textsf{R}[rs])$\;
    $data \gets Copy(\textsf{R}[rt])$\;
    \If{$\lnot code.P_I \lor code.O \neq data.O$}{
      $Trap(\texttt{InvokeError})$\;
    }
    \If{$code.O \And FFFFFF00_{16} = FFFFFF00_{16}$}{
      $Trap(\texttt{SealingError})$\;
    }
    $code.O \gets FFFFFFFF_{16}$\;
    $data.O \gets FFFFFFFF_{16}$\;
    $BranchLink(\textsf{R}[rd], code)$\;
    $\textsf{R}[31] \gets data$\;
  \end{pseudo}
  \desc Invoke a function represented by a code and a data sealed object
  capabilities. Both capabilities must be of the same type, and the code
  capability must be invokable. The data capability is placed in \textsf{A11}.
\end{instr}

\begin{instr}{LD.BS}
  \formatImmediateLoad{1001000}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(LoadScalar(\textsf{R}[rs], imm, 1)))$\;
  \end{pseudo}
  \desc Load signed byte.
\end{instr}

\begin{instr}{LD.BS.PC}
  \formatPCRel{1011000}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(LoadScalar(\textsf{PC}, imm, 1)))$\;
  \end{pseudo}
  \desc Load signed byte, relative to program counter.
\end{instr}

\begin{instr}{LD.BU}
  \formatImmediateLoad{1000000}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{R}[rs], imm, 1))$\;
  \end{pseudo}
  \desc Load unsigned byte.
\end{instr}

\begin{instr}{LD.BU.PC}
  \formatPCRel{1010000}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{PC}, imm, 1))$\;
  \end{pseudo}
  \desc Load unsigned byte, relative to program counter.
\end{instr}

\begin{instr}{LD.C}
  \formatImmediateLoad{1000101}
  \begin{pseudo}
    $\textsf{R}[rd] \gets LoadCapability(\textsf{R}[rs], imm)$\;
  \end{pseudo}
  \desc Load capability.
\end{instr}

\begin{instr}{LD.C.PC}
  \formatPCRel{1010101}
  \begin{pseudo}
    $\textsf{R}[rd] \gets LoadCapability(\textsf{PC}, imm)$\;
  \end{pseudo}
  \desc Load capability, relative to program counter.
\end{instr}

\begin{instr}{LD.DS}
  \formatImmediateLoad{1001010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(LoadScalar(\textsf{R}[rs], imm, 4)))$\;
  \end{pseudo}
  \desc Load signed doubleword.
\end{instr}

\begin{instr}{LD.DS.PC}
  \formatPCRel{1011010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(LoadScalar(\textsf{PC}, imm, 4)))$\;
  \end{pseudo}
  \desc Load signed doubleword, relative to program counter.
\end{instr}

\begin{instr}{LD.DU}
  \formatImmediateLoad{1000010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{R}[rs], imm, 4))$\;
  \end{pseudo}
  \desc Load unsigned doubleword.
\end{instr}

\begin{instr}{LD.DU.PC}
  \formatPCRel{1010010}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{PC}, imm, 4))$\;
  \end{pseudo}
  \desc Load unsigned doubleword, relative to program counter.
\end{instr}

\begin{instr}{LD.Q}
  \formatImmediateLoad{1000011}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{R}[rs], imm, 8))$\;
  \end{pseudo}
  \desc Load quadword.
\end{instr}

\begin{instr}{LD.Q.PC}
  \formatPCRel{1010011}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{PC}, imm, 8))$\;
  \end{pseudo}
  \desc Load quadword, relative to program counter.
\end{instr}

\begin{instr}{LD.WS}
  \formatImmediateLoad{1001001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(LoadScalar(\textsf{R}[rs], imm, 2)))$\;
  \end{pseudo}
  \desc Load signed word.
\end{instr}

\begin{instr}{LD.WS.PC}
  \formatPCRel{1011001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(LoadScalar(\textsf{PC}, imm, 2)))$\;
  \end{pseudo}
  \desc Load signed word, relative to program counter.
\end{instr}

\begin{instr}{LD.WU}
  \formatImmediateLoad{1000001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{R}[rs], imm, 2))$\;
  \end{pseudo}
  \desc Load unsigned word.
\end{instr}

\begin{instr}{LD.WU.PC}
  \formatPCRel{1010001}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], LoadScalar(\textsf{PC}, imm, 2))$\;
  \end{pseudo}
  \desc Load unsigned word, relative to program counter.
\end{instr}

\begin{instr}{NSELECT.U.I}
  \formatImmediate{0100101}
  \begin{pseudo}
    \eIf{$\textsf{C} = 1$}{
      $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A)$\;
    }{
      $SetScalar(\textsf{R}[rd], imm)$\;
    }
  \end{pseudo}
  \desc Scalar conditional selection, first operand if \textsf{C} is true, else
  second operand.
\end{instr}

\begin{instr}{OR.U}
  \formatRegister{1111111}{0000011}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Or \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar logical OR.
\end{instr}

\begin{instr}{OR.U.I}
  \formatImmediate{0000011}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Or imm)$\;
  \end{pseudo}
  \desc Scalar logical OR.
\end{instr}

\begin{instr}{SEAL.E}
  \formatRegisterRR{1111111}{0100101}
  \begin{pseudo}
    $sealed \gets Copy(\textsf{R}[rs])$\;
    \If{$sealed.O \neq FFFFFFFF_{16}$}{
      $Trap(\texttt{SealingError})$\;
    }
    $sealed.O \gets FFFFFFFE_{16}$\;
    $\textsf{R}[rd] \gets sealed$\;
  \end{pseudo}
  \desc Seal an entry capability.
\end{instr}

\begin{instr}{SEAL.O}
  \formatRegister{1111111}{0100100}
  \begin{pseudo}
    $sealer \gets Copy(\textsf{R}[rt])$\;
    $sealed \gets Copy(\textsf{R}[rs])$\;
    $CheckAccess(sealer, 0, sealer.P_{OS})$\;
    \If{$sealed.O \neq FFFFFFFF_{16}$}{
      $Trap(\texttt{SealingError})$\;
    }
    $sealed.O \gets sealer.A[31:0]$\;
    $\textsf{R}[rd] \gets sealed$\;
  \end{pseudo}
  \desc Seal an object capability.
\end{instr}

\begin{instr}{SELECT}
  \formatRegister{1111111}{0100011}
  \begin{pseudo}
    \eIf{$\textsf{C} = 0$}{
      $\textsf{R}[rd][256:0] \gets \textsf{R}[rs][256:0]$\;
    }{
      $\textsf{R}[rd][256:0] \gets \textsf{R}[rt][256:0]$\;
    }
  \end{pseudo}
  \desc Conditional selection, first operand if \textsf{C} is false, else second
  operand.
\end{instr}

\begin{instr}{SELECT.U.I}
  \formatImmediate{0100100}
  \begin{pseudo}
    \eIf{$\textsf{C} = 0$}{
      $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A)$\;
    }{
      $SetScalar(\textsf{R}[rd], imm)$\;
    }
  \end{pseudo}
  \desc Scalar conditional selection, first operand if \textsf{C} is false, else
  second operand.
\end{instr}

\begin{instr}{SET.A.C}
  \formatRegister{1111111}{0001011}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Set address of capability.
\end{instr}

\begin{instr}{SET.BL.C}
  \formatRegister{1111111}{0101101}
  \begin{pseudo}
    $cap \gets Copy(\textsf{R}[rs])$\;
    $newBnd \gets \textsf{R}[rt].A$\;
    \If{$newBnd < cap.B_L \lor newBnd > cap.B_U$}{
      $cap.T \gets 0$\;
    }
    $cap.B_L \gets newBnd$\;
    $\textsf{R}[rd] \gets cap$\;
  \end{pseudo}
  \desc Set lower bound of capability.
\end{instr}

\begin{instr}{SET.BU.C}
  \formatRegister{1111111}{0101100}
  \begin{pseudo}
    $cap \gets Copy(\textsf{R}[rs])$\;
    $newBnd \gets \textsf{R}[rt].A$\;
    \If{$newBnd < cap.B_L \lor newBnd > cap.B_U$}{
      $cap.T \gets 0$\;
    }
    $cap.B_U \gets newBnd$\;
    $\textsf{R}[rd] \gets cap$\;
  \end{pseudo}
  \desc Set upper bound of capability.
\end{instr}

\begin{instr}{SETA.C.I}
  \formatImmediate{0001011}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], imm)$\;
  \end{pseudo}
  \desc Set address of capability.
\end{instr}

\begin{instr}{SHL.U}
  \formatRegister{1111111}{0000101}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Shl \textsf{R}[rt].A[6:0])$\;
  \end{pseudo}
  \desc Scalar shift left unsigned.
\end{instr}

\begin{instr}{SHL.U.I}
  \formatImmediateShift{0000101}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Shl imm[6:0])$\;
  \end{pseudo}
  \desc Scalar shift left unsigned.
\end{instr}

\begin{instr}{SHR.S}
  \formatRegister{1111111}{0000111}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(\textsf{R}[rs].A) \Shr \textsf{R}[rt].A[6:0])$\;
  \end{pseudo}
  \desc Scalar shift right signed.
\end{instr}

\begin{instr}{SHR.S.I}
  \formatImmediateShift{0000111}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], Signed(\textsf{R}[rs].A) \Shr imm[6:0])$\;
  \end{pseudo}
  \desc Scalar shift right signed.
\end{instr}

\begin{instr}{SHR.U}
  \formatRegister{1111111}{0000110}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Shr \textsf{R}[rt].A[6:0])$\;
  \end{pseudo}
  \desc Scalar shift right unsigned.
\end{instr}

\begin{instr}{SHR.U.I}
  \formatImmediateShift{0000110}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Shr imm[6:0])$\;
  \end{pseudo}
  \desc Scalar shift right unsigned.
\end{instr}

\begin{instr}{ST.B}
  \formatStore{1100000}
  \begin{pseudo}
    $StoreScalar(\textsf{R}[rs], imm, 1, \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Store byte.
\end{instr}

\begin{instr}{ST.C}
  \formatStore{1100101}
  \begin{pseudo}
    $StoreCapability(\textsf{R}[rs], imm, \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Store capability.
\end{instr}

\begin{instr}{ST.D}
  \formatStore{1100010}
  \begin{pseudo}
    $StoreScalar(\textsf{R}[rs], imm, 4, \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Store doubleword.
\end{instr}

\begin{instr}{ST.Q}
  \formatStore{1100011}
  \begin{pseudo}
    $StoreScalar(\textsf{R}[rs], imm, 8, \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Store quadword.
\end{instr}

\begin{instr}{ST.W}
  \formatStore{1100001}
  \begin{pseudo}
    $StoreScalar(\textsf{R}[rs], imm, 2, \textsf{R}[rt])$\;
  \end{pseudo}
  \desc Store word.
\end{instr}

\begin{instr}{SUB.C}
  \formatRegister{1111111}{0001000}
  \begin{pseudo}
    $SetAddress(\textsf{R}[rd], \textsf{R}[rs], \textsf{R}[rs].A - \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Capability subtraction.
\end{instr}

\begin{instr}{SUB.U}
  \formatRegister{1111111}{0000000}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A - \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar subtraction.
\end{instr}

\begin{instr}{UNSEAL.E}
  \formatRegisterRR{1111111}{0100111}
  \begin{pseudo}
    $sealed \gets Copy(\textsf{R}[rs])$\;
    \If{$sealed.O \neq FFFFFFFE_{16}$}{
      $Trap(\texttt{SealingError})$\;
    }
    $sealed.O \gets FFFFFFFF_{16}$\;
    $\textsf{R}[rd] \gets sealed$\;
  \end{pseudo}
  \desc Unseal an entry capability.
\end{instr}

\begin{instr}{UNSEAL.O}
  \formatRegister{1111111}{0100110}
  \begin{pseudo}
    $sealer \gets Copy(\textsf{R}[rt])$\;
    $sealed \gets Copy(\textsf{R}[rs])$\;
    $CheckAccess(sealer, 0, sealer.P_{OU})$\;
    \If{$sealed.O \neq sealer.A[31:0]$}{
      $Trap(\texttt{SealingError})$\;
    }
    $sealed.O \gets FFFFFFFF_{16}$\;
    $\textsf{R}[rd] \gets sealed$\;
  \end{pseudo}
  \desc Unseal an object capability.
\end{instr}

\begin{instr}{XOR.U}
  \formatRegister{1111111}{0000100}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Xor \textsf{R}[rt].A)$\;
  \end{pseudo}
  \desc Scalar logical XOR.
\end{instr}

\begin{instr}{XOR.U.I}
  \formatImmediate{0000100}
  \begin{pseudo}
    $SetScalar(\textsf{R}[rd], \textsf{R}[rs].A \Xor imm)$\;
  \end{pseudo}
  \desc Scalar logical XOR.
\end{instr}

\section{Opcode tables}

\begin{table}[H]
  \centering
  \caption{$major$}
  \tiny
  \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
    \hline
    $major[2:0]$
    & \multirow{2}{*}{\makecell{$000_2$ \\ $0_8$}}
    & \multirow{2}{*}{\makecell{$001_2$ \\ $1_8$}}
    & \multirow{2}{*}{\makecell{$010_2$ \\ $2_8$}}
    & \multirow{2}{*}{\makecell{$011_2$ \\ $3_8$}}
    & \multirow{2}{*}{\makecell{$100_2$ \\ $4_8$}}
    & \multirow{2}{*}{\makecell{$101_2$ \\ $5_8$}}
    & \multirow{2}{*}{\makecell{$110_2$ \\ $6_8$}}
    & \multirow{2}{*}{\makecell{$111_2$ \\ $7_8$}}
    \\\cline{1-1}
    $major[6:3]$ & & & & & & & &
    \\\hline
    \makecell{$0000_2$ \\ $00_8$}
    & \cellcolor{lightgray}
    & \instrref{ADD.U.I}
    & \instrref{AND.U.I}
    & \instrref{OR.U.I}
    & \instrref{XOR.U.I}
    & \instrref{SHL.U.I}
    & \instrref{SHR.U.I}
    & \instrref{SHR.S.I}
    \\\hline
    \makecell{$0001_2$ \\ $01_8$}
    & \cellcolor{lightgray}
    & \instrref{ADD.C.I}
    & \instrref{AND.C.I}
    & \instrref{SETA.C.I}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \instrref{ANDP.C.I}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$0010_2$ \\ $02_8$}
    & \instrref{CMP.EQL.U.I}
    & \instrref{CMP.NEQ.U.I}
    & \instrref{CMP.AND.U.I}
    & \instrref{CMP.NAND.U.I}
    & \instrref{CMP.LT.U.I}
    & \instrref{CMP.GE.U.I}
    & \instrref{CMP.LT.S.I}
    & \instrref{CMP.GE.S.I}
    \\\hline
    \makecell{$0011_2$ \\ $03_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \instrref{CMP.ANDP.C.I}
    & \instrref{CMP.NANDP.C.I}
    & \instrref{CMP.GT.U.I}
    & \instrref{CMP.LE.U.I}
    & \instrref{CMP.GT.S.I}
    & \instrref{CMP.LE.S.I}
    \\\hline
    \makecell{$0100_2$ \\ $04_8$}
    & \instrref{BL}
    & \cellcolor{lightgray}
    & \instrref{BL.CF}
    & \instrref{BL.CT}
    & \instrref{SELECT.U.I}
    & \instrref{NSELECT.U.I}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$0101_2$ \\ $05_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$0110_2$ \\ $06_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$0111_2$ \\ $07_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1000_2$ \\ $10_8$}
    & \instrref{LD.BU}
    & \instrref{LD.WU}
    & \instrref{LD.DU}
    & \instrref{LD.Q}
    & \cellcolor{lightgray}
    & \instrref{LD.C}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1001_2$ \\ $11_8$}
    & \instrref{LD.BS}
    & \instrref{LD.WS}
    & \instrref{LD.DS}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1010_2$ \\ $12_8$}
    & \instrref{LD.BU.PC}
    & \instrref{LD.WU.PC}
    & \instrref{LD.DU.PC}
    & \instrref{LD.Q.PC}
    & \cellcolor{lightgray}
    & \instrref{LD.C.PC}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1011_2$ \\ $13_8$}
    & \instrref{LD.BS.PC}
    & \instrref{LD.WS.PC}
    & \instrref{LD.DS.PC}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1100_2$ \\ $14_8$}
    & \instrref{ST.B}
    & \instrref{ST.W}
    & \instrref{ST.D}
    & \instrref{ST.Q}
    & \cellcolor{lightgray}
    & \instrref{ST.C}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1101_2$ \\ $15_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1110_2$ \\ $16_8$}
    & \instrref{ADD.PC.I}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1111_2$ \\ $17_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & $*$
    \\\hline
  \end{tabular}
\end{table}

\begin{table}[H]
  \centering
  \caption{$major = 1111111_2, minor$}
  \tiny
  \begin{tabular}{|c|c|c|c|c|c|c|c|c|}
    \hline
    $minor[2:0]$
    & \multirow{2}{*}{\makecell{$000_2$ \\ $0_8$}}
    & \multirow{2}{*}{\makecell{$001_2$ \\ $1_8$}}
    & \multirow{2}{*}{\makecell{$010_2$ \\ $2_8$}}
    & \multirow{2}{*}{\makecell{$011_2$ \\ $3_8$}}
    & \multirow{2}{*}{\makecell{$100_2$ \\ $4_8$}}
    & \multirow{2}{*}{\makecell{$101_2$ \\ $5_8$}}
    & \multirow{2}{*}{\makecell{$110_2$ \\ $6_8$}}
    & \multirow{2}{*}{\makecell{$111_2$ \\ $7_8$}}
    \\\cline{1-1}
    $minor[6:3]$ & & & & & & & &
    \\\hline
    \makecell{$0000_2$ \\ $00_8$}
    & \instrref{SUB.U}
    & \instrref{ADD.U}
    & \instrref{AND.U}
    & \instrref{OR.U}
    & \instrref{XOR.U}
    & \instrref{SHL.U}
    & \instrref{SHR.U}
    & \instrref{SHR.S}
    \\\hline
    \makecell{$0001_2$ \\ $01_8$}
    & \instrref{SUB.C}
    & \instrref{ADD.C}
    & \instrref{AND.C}
    & \instrref{SET.A.C}
    & \instrref{GET.BU.C}
    & \instrref{GET.BL.C}
    & \instrref{ANDP.C}
    & \instrref{GET.F.C}
    \\\hline
    \makecell{$0010_2$ \\ $02_8$}
    & \instrref{CMP.EQL.U}
    & \instrref{CMP.NEQ.U}
    & \instrref{CMP.AND.U}
    & \instrref{CMP.NAND.U}
    & \instrref{CMP.LT.U}
    & \instrref{CMP.GE.U}
    & \instrref{CMP.LT.S}
    & \instrref{CMP.GE.S}
    \\\hline
    \makecell{$0011_2$ \\ $03_8$}
    & \instrref{CMP.EQL.C}
    & \instrref{CMP.NEQ.C}
    & \instrref{CMP.ANDP.C}
    & \instrref{CMP.NANDP.C}
    & \instrref{CMP.TAG}
    & \instrref{CMP.NTAG}
    & \instrref{CMP.SEAL}
    & \instrref{CMP.NSEAL}
    \\\hline
    \makecell{$0100_2$ \\ $04_8$}
    & \instrref{BLR}
    & \instrref{BLR.NS}
    & \instrref{INVK}
    & \instrref{SELECT}
    & \instrref{SEAL.O}
    & \instrref{UNSEAL.O}
    & \instrref{SEAL.E}
    & \instrref{UNSEAL.E}
    \\\hline
    \makecell{$0101_2$ \\ $05_8$}
    & \cellcolor{lightgray}
    & \instrref{BSWAP.W}
    & \instrref{BSWAP.D}
    & \instrref{BSWAP.Q}
    & \instrref{SET.BU.C}
    & \instrref{SET.BL.C}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$0110_2$ \\ $06_8$}
    & \instrref{EXT.BU}
    & \instrref{EXT.WU}
    & \instrref{EXT.DU}
    & \cellcolor{lightgray}
    & \instrref{EXT.BS}
    & \instrref{EXT.WS}
    & \instrref{EXT.DS}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$0111_2$ \\ $07_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1000_2$ \\ $10_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1001_2$ \\ $11_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1010_2$ \\ $12_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1011_2$ \\ $13_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1100_2$ \\ $14_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1101_2$ \\ $15_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1110_2$ \\ $16_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
    \makecell{$1111_2$ \\ $17_8$}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    & \cellcolor{lightgray}
    \\\hline
  \end{tabular}
\end{table}
