; fasmg include file for Carmel.

include "fasmg-stuff/utility/xcalm.inc"
include "fasmg-stuff/utility/once.inc"
once

calminstruction _emit_be_num? size*, arg*
  local i, buffer
  compute i, size
arg_loop:
  check i = 0
  jyes arg_end
  compute i, i - 1

  local tmp
  compute tmp, (arg shr (i * 8)) and 0xFF
  asmarranged tmp, =emit 1 : tmp

  jump arg_loop
arg_end:
end calminstruction

calminstruction _emit_be_one? size*, arg*
  match =?, arg
  jyes reserve

  check arg eqtype ""
  jyes string

  xcall _emit_be_num, size, arg
  exit

string:
  check arg = 0
  jyes fin

  local char
  compute char, arg and 0xFF
  xcall _emit_be_num, size, char

  compute arg, arg shr 8
  jump string

reserve:
  local buffer
  asmarranged buffer, =rb 1

fin:
end calminstruction

; Big-endian form of emit.
calminstruction emit_be? args&
  local size
  match size:args, args
  jyes cont
  match size=,args, args
cont:
  compute size, size

process_args:
  match , args
  jyes fin

  local arg
  match arg=,args, args
  jyes argument

  arrange arg, args
  arrange args,

argument:
  local repn
  compute repn, 1
  match repn =dup? arg, arg

  compute repn, repn

rep_loop:
  check repn <= 0
  jyes process_args
  compute repn, repn - 1

  xcall _emit_be_one, size, arg
  jump rep_loop

fin:
end calminstruction

; Redefine basic data instructions to be big-endian.
iterate <name,size>, db,1, dw,2, dd,4, dq,8
  calminstruction name? args&
    local buffer
    asmarranged buffer, =emit_be size : args
  end calminstruction
end iterate

calminstruction align? n
  compute n, (n - ($ mod n)) mod n
  local buffer
  asmarranged buffer, =db n =dup ?
end calminstruction

define carmel carmel

element carmel.r

repeat 64, i : 0
  element R#i? : carmel.r + i
end repeat

NULL? = R0
RA? = R1
SP? = R2
GP? = R3
TP? = R4
repeat 3, i : 0, j : 5
  K#i? = R#j
end repeat
repeat 8, i : 0, j : 8
  T#i? = R#j
end repeat
repeat 32, i : 0, j : 16
  V#i? = R#j
end repeat
repeat 16, i : 0, j : 48
  A#i? = R#j
end repeat

carmel.tmp = 0

calminstruction carmel.add_field field*, value*
  compute carmel.tmp, carmel.tmp or (value shl (field * 6))
end calminstruction

calminstruction carmel.emit
  local buffer, tmp
  compute tmp, carmel.tmp
  asmarranged buffer, =emit_be 4 : tmp
  compute carmel.tmp, 0
end calminstruction

calminstruction carmel.check_reg field*, operand*
  check operand scale 0 = 0 & operand scale 1 = 1 & \
        operand metadata 1 relativeto carmel.r
  compute operand, operand metadata 1 scale 0
  xcall carmel.add_field, field, operand
end calminstruction

calminstruction carmel.check_val size*, imm*
  compute imm, imm
  check (imm >= -(1 shl (size - 1)) & \
         imm < (1 shl (size - 1))) | \
        (imm >= (1 shl 64) - (1 shl (size - 1)) & \
         imm < (1 shl 64))
  jno error

  compute carmel.imm, imm and ((1 shl size) - 1)
  exit

error:
  asm err "value out of range"
end calminstruction

calminstruction carmel.imm_field tgt*, src*, imm*
  local tmp
  compute tmp, (imm shr (src * 6)) and 63
  xcall carmel.add_field, tgt, tmp
end calminstruction

calminstruction carmel.instr_r major*, minor*, s*, t*, d*
  xcall carmel.add_field, (4), major
  xcall carmel.check_reg, (3), d
  xcall carmel.check_reg, (2), s
  xcall carmel.check_reg, (1), t
  xcall carmel.add_field, (0), minor
  xcall carmel.emit
end calminstruction

calminstruction carmel.instr_i major*, s*, imm*, d*
  match #imm, imm
  jno error
  compute imm, imm

  xcall carmel.check_val, (12), imm
  xcall carmel.add_field, (4), major
  xcall carmel.check_reg, (3), d
  xcall carmel.check_reg, (2), s
  xcall carmel.imm_field, (1), (1), imm
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit

error:
  asm err "invalid immediate"
end calminstruction

calminstruction carmel.instr_im major*, mem*, d*
  local s, imm
  match imm(s), mem
  jyes cont
  match (s), mem
  compute imm, 0
  jno error
cont:
  compute imm, imm

  xcall carmel.check_val, (12), imm
  xcall carmel.add_field, (4), major
  xcall carmel.check_reg, (3), d
  xcall carmel.check_reg, (2), s
  xcall carmel.imm_field, (1), (1), imm
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit

error:
  asm err "invalid memory reference"
end calminstruction

calminstruction carmel.instr_sm major*, t*, mem*
  local s, imm
  match imm(s), mem
  jyes cont
  match (s), mem
  compute imm, 0
  jno error
cont:
  compute imm, imm

  xcall carmel.check_val, (12), imm
  xcall carmel.add_field, (4), major
  xcall carmel.imm_field, (3), (1), imm
  xcall carmel.check_reg, (2), s
  xcall carmel.check_reg, (1), t
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit

error:
  asm err "invalid memory reference"
end calminstruction

calminstruction carmel.instr_bt major*, target*
  local imm
  compute imm, (target - $ - 4) / 4

  xcall carmel.check_val, (24), imm
  xcall carmel.add_field, (4), major
  xcall carmel.imm_field, (3), (3), imm
  xcall carmel.imm_field, (2), (2), imm
  xcall carmel.imm_field, (1), (1), imm
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit
end calminstruction

calminstruction carmel.instr_lt major*, target*, d*
  local imm
  compute imm, (target - $ - 4) / 4

  xcall carmel.check_val, (18), imm
  xcall carmel.add_field, (4), major
  xcall carmel.check_reg, (3), d
  xcall carmel.imm_field, (2), (2), imm
  xcall carmel.imm_field, (1), (1), imm
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit
end calminstruction

calminstruction carmel.instr_lm major*, mem*, d*
  local imm
  match (imm), mem
  jno error
  compute imm, imm - $ - 4

  xcall carmel.check_val, (18), imm
  xcall carmel.add_field, (4), major
  xcall carmel.check_reg, (3), d
  xcall carmel.imm_field, (2), (2), imm
  xcall carmel.imm_field, (1), (1), imm
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit

error:
  asm err "invalid memory reference"
end calminstruction

calminstruction carmel.instr_xm major*, t*, mem*
  local imm
  match (imm), mem
  jno error
  compute imm, imm - $ - 4

  xcall carmel.check_val, (18), imm
  xcall carmel.add_field, (4), major
  xcall carmel.imm_field, (3), (2), imm
  xcall carmel.imm_field, (2), (1), imm
  xcall carmel.check_reg, (1), t
  xcall carmel.imm_field, (0), (0), imm
  xcall carmel.emit
  exit

error:
  asm err "invalid memory reference"
end calminstruction

macro carmel.conditional name*
  define name? name?

  iterate <cond,n>, ,01b, .NC?,10b, .C?,11b
    calminstruction name?cond args&
      xcall carmel.add_field, (5), (n)
      local buffer
      asmarranged buffer, =name.=SYS args
    end calminstruction
  end iterate
end macro

iterate <name,minor,type>, \
        SUB,00q,std, ADD,01q,std, AND,02q,std, OR,03q,std, \
        XOR,04q,std, SHL,05q,std, SHR,06q,std, SHRU,07q,std, \
        CEQ,10q,std, CLT,11q,std, CLTU,12q,std, CTEST,13q,std, \
        CNE,14q,std, CGE,15q,std, CGEU,16q,std, CNTEST,17q,std, \
        JLR,21q,sd1, JLRX,23q,sd1, \
        BSWAPW,25q,sd1, BSWAPD,26q,sd1, BSWAPQ,27q,sd1, \
        EXTB,30q,sd1, EXTW,31q,sd1, EXTD,32q,sd1, \
        EXTBU,34q,sd1, EXTWU,35q,sd1, EXTDU,36q,sd1, \
        CNTC,54q,sd1, CNTS,55q,sd1, \
        CTZ,60q,sd1, CTO,61q,sd1, CLZ,62q,sd1, CLO,63q,sd1, \
        MOVNC,64q,d, MOVNCU,65q,d, MOVC,66q,d, MOVCU,67q,d, \
        ANDN,72q,std, ORN,73q,std, XORN,74q,std
  define name? name?
  carmel.conditional name

  match =std, type
    calminstruction name?.SYS? s*, t*, d*
      xcall carmel.instr_r, (0), (minor), s, t, d
    end calminstruction
  else match =sd1, type
    calminstruction name?.SYS? s*, d*
      xcall carmel.instr_r, (0), (minor), s, NULL, d
    end calminstruction
  else match =st, type
    calminstruction name?.SYS? s*, t*
      xcall carmel.instr_r, (0), (minor), s, t, NULL
    end calminstruction
  else match =s, type
    calminstruction name?.SYS? s*
      xcall carmel.instr_r, (0), (minor), s, NULL, NULL
    end calminstruction
  else match =d, type
    calminstruction name?.SYS? d*
      xcall carmel.instr_r, (0), (minor), NULL, NULL, d
    end calminstruction
  end match
end iterate

iterate <name,major,type>, \
        ADDI,01q,i, ANDI,02q,i, ORI,03q,i, \
        XORI,04q,i, SHLI,05q,i, SHRI,06q,i, SHRUI,07q,i, \
        CEQI,10q,i, CLTI,11q,i, CLTUI,12q,i, CTESTI,13q,i, \
        CNEI,14q,i, CGEI,15q,i, CGEUI,16q,i, CNTESTI,17q,i, \
        B,20q,bt, BL,21q,lt, BX,22q,bt, BLX,23q,lt, \
        SB,24q,sm, SW,25q,sm, SD,26q,sm, SQ,27q,sm, \
        LB,30q,im, LW,31q,im, LD,32q,im, LQ,33q,im, \
        LBU,34q,im, LWU,35q,im, LDU,36q,im, \
        CGTI,51q,i, CGTIU,52q,i, \
        CLEI,55q,i, CLEIU,56q,i, \
        SBPC,64q,xm, SWPC,65q,xm, SDPC,66q,xm, SQPC,67q,xm, \
        LBPC,70q,lm, LWPC,71q,lm, LDPC,72q,lm, LQPC,73q,lm, \
        LBUPC,74q,lm, LWUPC,75q,lm, LDUPC,76q,lm, LAPC,77q,lm
  carmel.conditional name

  match =i, type
    calminstruction name?.SYS? s*, imm*, d*
      xcall carmel.instr_i, (major), s, imm, d
    end calminstruction
  else match =im, type
    calminstruction name?.SYS? mem*, d*
      xcall carmel.instr_im, (major), mem, d
    end calminstruction
  else match =sm, type
    calminstruction name?.SYS? t*, mem*
      xcall carmel.instr_sm, (major), t, mem
    end calminstruction
  else match =bt, type
    calminstruction name?.SYS? target*
      xcall carmel.instr_bt, (major), target
    end calminstruction
  else match =lt, type
    calminstruction name?.SYS? target*, d*
      xcall carmel.instr_lt, (major), target, d
    end calminstruction
  else match =lm, type
    calminstruction name?.SYS? mem*, d*
      xcall carmel.instr_lm, (major), mem, d
    end calminstruction
  else match =xm, type
    calminstruction name?.SYS? t*, mem*
      xcall carmel.instr_xm, (major), t, mem
    end calminstruction
  end match
end iterate

carmel.conditional CGT
macro CGT?.SYS? s*, t*, d*
  CLT.SYS t, s, d
end macro

carmel.conditional CGTU
macro CGTU?.SYS? s*, t*, d*
  CLTU.SYS t, s, d
end macro

carmel.conditional CLE
macro CLE?.SYS? s*, t*, d*
  CGE.SYS t, s, d
end macro

carmel.conditional CLEU
macro CLEU?.SYS? s*, t*, d*
  CGEU.SYS t, s, d
end macro

carmel.conditional MOV
macro MOV?.SYS? s*, d*
  SUB.SYS s, NULL, d
end macro

carmel.conditional NEG
macro NEG?.SYS? s*, d*
  SUB.SYS NULL, s, d
end macro

carmel.conditional NOT
macro NOT?.SYS? s*, d*
  XORI.SYS s, #-1, d
end macro

carmel.conditional MOVI
macro MOVI?.SYS? imm*, d*
  ADDI.SYS NULL, imm, d
end macro

carmel.conditional NOP
macro NOP?.SYS?
  SUB.SYS NULL, NULL, NULL
end macro

end once
